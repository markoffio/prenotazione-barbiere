//
//  PrenotazioneBarbiereApp.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 12/09/21.
//

import SwiftUI
import Firebase

@main
struct PrenotazioneBarbiereApp: App {
        
    init() {
        // Customize navigation appearance
        let navigationBarAppearance = UINavigationBarAppearance()
        navigationBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.lightBlack, .font: UIFont(name: "LexendDeca-Bold", size: 30)!]
        navigationBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.lightBlack, .font: UIFont(name: "LexendDeca-Bold", size: 22)!]
        navigationBarAppearance.backgroundColor = .white
        navigationBarAppearance.backgroundEffect = .none
        navigationBarAppearance.shadowColor = .clear
        
        UINavigationBar.appearance().standardAppearance = navigationBarAppearance
        UINavigationBar.appearance().scrollEdgeAppearance = navigationBarAppearance
        UINavigationBar.appearance().compactAppearance = navigationBarAppearance
        
        // Customize tab bar
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont.init(name: "LexendDeca-Regular", size: 12)! ], for: .normal)
        UITabBar.appearance().barTintColor = .white
        
        // Configure Firebase
        FirebaseApp.configure()
    }
    
    var body: some Scene {
        WindowGroup {
            ContentView().environmentObject(AuthenticationViewModel.shared)
        }
    }
}
