//
//  Constants.swift
//  Tog
//
//  Created by Marco Margarucci on 24/09/21.
//

import Foundation
import Firebase

// MARK: UI


// Week days
let days: [String] = ["D", "L", "M", "M", "G", "V", "S"]


// MARK: - Authentication
let minimumPasswordLength: Int = 6

// MARK: - Navigation titles
// Services list view navigation title
let servicesListViewNavigationTitle: String = "I nostri servizi"

// MARK: - Tab bar
// Reservation title
let reservationTitle: String = "Prenota"
// Services title
let servicesTitle: String = "Servizi"
// Settings title
let settingsTitle: String = "Impostazioni"
// Reservation image
let reservationImage: String = "calendar"
// Services image
let servicesImage: String = "list.bullet"
// Settings image
let settingsImage: String = "gear"
