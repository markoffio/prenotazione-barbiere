//
//  CustomerReservationValidator.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 11/11/21.
//

import Foundation

struct CustomerReservationValidator {
    func isValid(selectedServices: [[String: Int]]) -> Bool {
        guard !selectedServices.isEmpty else { return false }
        return true
    }
}
