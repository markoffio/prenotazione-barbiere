//
//  AlertItem.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 09/10/21.
//

import SwiftUI

struct AlertItem: Identifiable {
    let id = UUID()
    let title: Text
    let message: Text
    let dismissButton: Alert.Button
    
    var alert: Alert {
        Alert(title: title, message: message, dismissButton: dismissButton)
    }
}

struct AlertContext {
    // MARK: - Selected date error
    
    // Unable to make reservation for selected date
    static let unableToMakeReservationSelectedDate = AlertItem(title: Text("Data non disponibile"), message: Text("Non è possibile effettuare una prenotazione nella data selezionata. Seleziona un giorno compreso fra martedì e sabato."), dismissButton: .default(Text("OK")))
    
    // Selected date no time slots available
    static let noTimeSlotsAvailable = AlertItem(title: Text("Impossibile prenotarsi"), message: Text("Non è possibile effettuare una prenotazione nella data selezionata. Tutti gli orari disponibili sono già occupati."), dismissButton: .default(Text("OK")))
    
    // No date or time slot selected
    static let noDateOrTimeSlotSelected = AlertItem(title: Text("Impossibile prenotarsi"), message: Text("Devi selezionare una data ed un orario prima di prenotarti."), dismissButton: .default(Text("OK")))
    
    // MARK: - Forgot password
    
    // Forgot password information
    static let forgotPasswordInformation = AlertItem(title: Text("Recupero password"), message: Text("Inserendo l'indirizzo email usato in fase di registrazione, riceverai via email le informazioni per modificare la password."), dismissButton: .default(Text("OK")))
        
    
    // Password reset intructions sent
    static let passwordResetInstructionsSent = AlertItem(title: Text("Recupero password"), message: Text("Le istruzioni per recuperare la password, sono state inviate all'indirizzo email specificato."), dismissButton: .default(Text("OK")))
}
