//
//  ClearTextFieldButton.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 02/07/22.
//

import SwiftUI

struct ClearTextFieldButton: View {
    
    @Binding var text: String
    
    var body: some View {
        HStack {
            Spacer()
            Image(systemName: "multiply.circle.fill")
                .foregroundColor(.secondary)
                .opacity(text == "" ? 0 : 1)
                .onTapGesture { self.text = "" }
        }
    }
}
