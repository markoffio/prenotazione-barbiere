//
//  NetworkManager.swift
//  NetworkManager
//
//  Created by Marco Margarucci on 29/11/21.
//

import Foundation
import Network

class NetworkManager: ObservableObject {
    // MARK: - Properties
    
    let monitor = NWPathMonitor()
    let queue = DispatchQueue(label: "NetworkManager")
    @Published var isConnected: Bool = true
    
    init() {
        monitor.pathUpdateHandler = { path in
            DispatchQueue.main.async {
                self.isConnected = path.status == .satisfied
            }
        }
        monitor.start(queue: queue)
    }
}
