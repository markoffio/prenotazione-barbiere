//
//  Font+Extensions.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 23/02/22.
//

import Foundation
import SwiftUI

extension Font {
    public static var body: Font {
        return Font.custom(FontName.regular, size: 16)
    }
    
    public static var button: Font {
        return Font.custom(FontName.semiBold, size: 14)
    }
    
    public static var tabBar: Font {
        return Font.custom(FontName.regular, size: 12)
    }
    
    public static var settings: Font {
        return Font.custom(FontName.regular, size: 16)
    }
    
    public static var settingsRow: Font {
        return Font.custom(FontName.regular, size: 13)
    }
    
    public static var calendar: Font {
        return Font.custom(FontName.semiBold, size: 15)
    }
    
    public static var title: Font {
        return Font.custom(FontName.bold, size: 20)
    }
    
    public static var subTitle: Font {
        return Font.custom(FontName.semiBold, size: 16)
    }
    
    public static var infoTitle: Font {
        return Font.custom(FontName.semiBold, size: 14)
    }
    
    public static var info: Font {
        return Font.custom(FontName.semiBold, size: 11)
    }
    
    public static var serviceDescription: Font {
        return Font.custom(FontName.semiBold, size: 13)
    }
}
