//
//  Color+Extension.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 21/08/21.
//

import Foundation
import SwiftUI

extension Color {
    // Background
    static let background = Color("background")
    // Deep blue
    static let deepBlue = Color("deepBlue")
    // Light blue
    static let lightBlue = Color("lightBlue")
    // Light black
    static let lightBlack = Color("lightBlack")
    // Light gray
    static let lightGray = Color("lightGray")
    // Title red
    static let titleRed = Color("titleRed")
}
