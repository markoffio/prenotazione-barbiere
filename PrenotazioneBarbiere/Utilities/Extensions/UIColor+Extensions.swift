//
//  UIColor+Extensions.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 06/12/21.
//

import Foundation
import UIKit

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        let redValue = CGFloat(red) / 255.0
        let greenValue = CGFloat(green) / 255.0
        let blueValue = CGFloat(blue) / 255.0
        self.init(red: redValue, green: greenValue, blue: blueValue, alpha: 1.0)
    }
    
    // Light black
    static let lightBlack = UIColor(red: 42, green: 48, blue: 50)
}
