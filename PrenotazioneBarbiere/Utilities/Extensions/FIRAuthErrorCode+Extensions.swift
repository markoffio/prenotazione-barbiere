//
//  FIRAuthErrorCode+Extensions.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 06/11/21.
//

import Firebase
import FirebaseAuth

extension AuthErrorCode {
    var errorMessage: String {
        switch self {
        case .emailAlreadyInUse:
            return "L'indirizzo email fornito è utilizzato da un altro utente."
        case .userNotFound:
            return "Account non presente. Le credenziali inserite non corrispondono ad alcun utente registrato nel sistema."
        case .userDisabled:
            return "Il tuo account è stato disabilitato. Contatta l'amministratore di sistema."
        case .missingEmail:
            return "Non hai inserito alcun indirizzo email. Per favore, inserisci un indirizzo email valido."
        case .invalidEmail, .invalidSender, .invalidRecipientEmail:
            return "Inserisci un indirizzo email valido."
        case .networkError:
            return "Errore di rete. Riprovare in seguito."
        case .weakPassword:
            return "Password troppo debole. La lunghezza della password deve essere di 8 caratteri o più."
        case .wrongPassword:
            return "La password inserita non è corretta. Se non ricordi la password, effettua la procedura di recupero."
        default:
            return "Errore sconosciuto."
        }
    }
}
