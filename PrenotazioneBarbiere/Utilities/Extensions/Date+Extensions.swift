//
//  Date+Extension.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 09/10/21.
//

import Foundation

extension Date {
    func getAllDates() -> [Date] {
        let calendar = Calendar.current
        let startDate = calendar.date(from: Calendar.current.dateComponents([.month, .year], from: self))!
        let range = calendar.range(of: .day, in: .month, for: startDate)!
        // Getting date
        return range.compactMap { day -> Date in
            return calendar.date(byAdding: .day, value: day - 1, to: self)!
        }
    }
    
    public var removeTimeStamp : Date? {
        guard let date = Calendar.current.date(from: Calendar.current.dateComponents([.day, .month], from: self)) else { return nil }
        return date
    }
    
    public var toString: String {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "it")
        formatter.dateFormat = "d MMMM y"
        return formatter.string(from: self)
    }
    
    public var hour: String {
        let formatter = DateFormatter()
        formatter.locale = Locale(identifier: "it")
        formatter.dateFormat = "HH:mm"
        return formatter.string(from: self)
    }
}
