//
//  UINavigationController+Extension.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 09/10/21.
//

import Foundation
import UIKit

extension UINavigationController {

  open override func viewWillLayoutSubviews() {
    navigationBar.topItem?.backButtonDisplayMode = .minimal
  }
}

