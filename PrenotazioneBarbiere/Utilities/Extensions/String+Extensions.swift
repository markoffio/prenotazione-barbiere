//
//  String+Extensions.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 13/12/21.
//

import Foundation

extension String {
    
    // Check if the email is valid
    func isEmailValid() -> Bool {
        // RFC 5322 Official Standard
        let emailRegex =
            "(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
            + "~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
            + "x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
            + "z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]"
            + "|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
            + "9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
            + "-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])"
        
        let emailPredicate = NSPredicate(format: "SELF MATCHES[c] %@", emailRegex)
        return emailPredicate.evaluate(with: self)
    }
    
    // Check if the password is valid
    func isPasswordValid() -> Bool {
        let passwordRegex = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*()\\-_=+{}|?>.<,:;~`’]{8,}$"
        let passwordPedicate = NSPredicate(format: "SELF MATCHES %@", passwordRegex)
        return passwordPedicate.evaluate(with: self)
    }
    
    // Check if the phone number is valid
    func isPhoneNumberValid() -> Bool {
        let phoneRegex = "^\\s*(?:\\+?(\\d{1,3}))?[-. (]*(\\d{3})[-. )]*(\\d{3})[-. ]*(\\d{4})(?: *x(\\d+))?\\s*$"
        let phonePredicate = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
        return phonePredicate.evaluate(with: self)
    }
    
    // Check if it is uppercased
    func isUpperCase() -> Bool {
        let upperCaseRegex = ".*[A-Z]+.*"
        return NSPredicate(format: "SELF MATCHES %@", upperCaseRegex).evaluate(with: self)
    }
    
    // Check if it is lowecased
    func isLowerCase() -> Bool {
        let lowerCaseRegex = ".*[a-z]+.*"
        return NSPredicate(format: "SELF MATCHES %@", lowerCaseRegex).evaluate(with: self)
    }
    
    // Check if contains characters
    func containsCharacters() -> Bool {
        let characterRegex = ".*[!@#$%^&*()\\-_=+{}|?>.<]+.*"
        return NSPredicate(format: "SELF MATCHES %@", characterRegex).evaluate(with: self)
    }
    
    // Check if contais digits
    func containsDigits() -> Bool {
        let digitRegex = ".*[0-9]+.*"
        return NSPredicate(format: "SELF MATCHES $@", digitRegex).evaluate(with: self)
    }
}
