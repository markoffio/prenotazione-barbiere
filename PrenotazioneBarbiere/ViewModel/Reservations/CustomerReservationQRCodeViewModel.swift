//
//  CustomerReservationQRCodeViewModel.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 02/12/21.
//

import SwiftUI
import CoreImage.CIFilterBuiltins

final class CustomerReservationQRCodeViewModel: ObservableObject {
    // Generate QR code
    func generateQRCode(from string: String) -> UIImage {
        let context = CIContext()
        let filter = CIFilter.qrCodeGenerator()
        let data = Data(string.utf8)
        filter.setValue(data, forKey: "inputMessage")
        
        if let outputImage = filter.outputImage {
            if let cgImage = context.createCGImage(outputImage, from: outputImage.extent) {
                return UIImage(cgImage: cgImage)
            }
        }
        
        return UIImage(systemName: "xmark.circle") ?? UIImage()
    }
}
