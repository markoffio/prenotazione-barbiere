//
//  CustomerReservationDetailsLocalViewModel.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 08/12/21.
//

import Foundation
import CoreData
import UIKit

class CustomerReservationDetailsLocalViewModel: ObservableObject {
    
    let container: NSPersistentContainer
    
    // Saved local reservations
    @Published var savedLocalReservations: [CustomerReservationLocalEntity] = []
    
    init() {
        container = NSPersistentContainer(name: "CustomerReservationLocal")
        container.loadPersistentStores { description, error in
            if let error = error {
                debugPrint("[ERROR]: Error loading core data: \(error.localizedDescription).")
            }
        }
    }
    
    // MARK: - Function
    
    // Fetch local reservations
    func fetchCustomerReservationsLocal() -> Int {
        let request = NSFetchRequest<CustomerReservationLocalEntity>(entityName: "CustomerReservationLocalEntity")
        let sort = NSSortDescriptor(key: "selectedDate", ascending: false)
        request.sortDescriptors = [sort]
        
        do {
            savedLocalReservations = try container.viewContext.fetch(request)
        } catch let error {
            debugPrint("[ERROR]: Error fetching local reservations: \(error.localizedDescription)")
        }
        return savedLocalReservations.count
    }
    
    // Save local reservations
    func saveReservations(customerId: String, date: Date, selectedDate: Date, selectedServices: [[String: Int]], time: String, totalPrice: Int, qrCodeInputString: String) {
        let newCustomerReservationLocal = CustomerReservationLocalEntity(context: container.viewContext)
        
        newCustomerReservationLocal.customerId = customerId
        newCustomerReservationLocal.date = date
        newCustomerReservationLocal.selectedDate = selectedDate
        newCustomerReservationLocal.selectedServices = selectedServices
        newCustomerReservationLocal.time = time
        newCustomerReservationLocal.totalPrice = Int32(totalPrice)
        newCustomerReservationLocal.qrCodeInputString = qrCodeInputString
        
        // Save data
        do {
            try container.viewContext.save()
        } catch let error {
            debugPrint("[ERROR]: Error saving to core date: \(error.localizedDescription)")
        }
    }

    // Delete local reservation
    func deleteCustomerReservationLocal(savedLocalReservations: CustomerReservationLocalEntity) -> Bool {
        container.viewContext.delete(savedLocalReservations)
        var deleted: Bool = false
        do {
            try container.viewContext.save()
            deleted = true
        } catch {
            debugPrint("*** [DEBUG]*** : Error deleting local reservation")
            deleted = false
        }
        return deleted
    }
}
