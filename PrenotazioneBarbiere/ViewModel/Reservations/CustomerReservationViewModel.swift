//
//  CustomerReservationViewModel.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 14/11/21.
//

import Foundation

class CustomerReservationViewModel: ObservableObject {
    // Selected services
    @Published var selectedServices = [[String: Int]]()
    // Create customer reservartion success
    @Published var createCustomerReservationSuccess: Bool = false
    // Error
    @Published var errorOccurred: Bool = false
    // Create custom reservation error
    @Published var createCustomerReservationError: String = ""
    // Loading
    @Published var loading: Bool = false
    // Customer reservation service
    private let customerReservationService = CustomerReservationService()
    // Customer reservation validator
    private let customerReservationValidator = CustomerReservationValidator()
    
    // Set customer reservation detail
    func setCustomerReservationDetail(customerReservation: CustomerReservation) {
        // Check if the customer has selected almost one service from the list
        if !customerReservationValidator.isValid(selectedServices: customerReservation.selectedServices) {
            errorOccurred = true
            createCustomerReservationError = CustomerReservationMessage.createCustomerReservationError
            return
        }
        loading = true
        customerReservationService.createCustomerReservationEntry(customerReservation: customerReservation) { [self]result in
            switch result {
            case .success:
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    self.loading = false
                    self.createCustomerReservationSuccess = true
                }
            case .failure(let error):
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    self.createCustomerReservationSuccess = false
                    self.errorOccurred = true
                    self.createCustomerReservationError = error.localizedDescription
                }
                return
            }
        }
    }
}
