//
//  ReservationDetailViewModel.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 09/10/21.
//

import Foundation

final class ReservationDetailViewModel: NSObject, ObservableObject {
    // MARK: - Properties
    // Reservation
    var reservation: Reservation
    // Is showing reservation detail view
    @Published var isShowingReservationDetailView: Bool = false
    
    init(reservation: Reservation) {
        self.reservation = reservation
    }
}
