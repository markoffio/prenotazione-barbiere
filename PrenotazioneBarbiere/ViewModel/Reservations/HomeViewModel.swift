//
//  HomeViewModel.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 09/10/21.
//

import Foundation
import SwiftUI

final class HomeViewModel: NSObject, ObservableObject {
    // MARK: - Properties
    // Used to check if the selected date is correct
    @Published var isCorrectDate: Bool = false
    // Alert item
    @Published var alertItem: AlertItem?
    // Used to show reservation detail view
    @Published var isShowingReservationDetailView: Bool = false
    // Current date
    var currentDate: Date = Date()
    // Current month
    var currentMonth: Int = 0
    
    func getCurrentDayAsNumber() -> Int? {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd"
        guard let dayAsNumber = Int(formatter.string(from: currentDate)) else { return nil }
        return dayAsNumber
    }
    
    // Check selected date
    func checkSelectedDate() {
        if !isCorrectDate {
            alertItem = AlertContext.unableToMakeReservationSelectedDate
        }
        else {
            debugPrint("Effettua prenotazione")
        }
    }
    
    // Check if same day
    func isSameDay(date1: Date, date2: Date) -> Bool {
        let calendar = Calendar.current
        return calendar.isDate(date1, inSameDayAs: date2)
    }
    
    func isSelectedDay(date: Date) -> Bool {
        let calendar = Calendar.current
        return calendar.isDate(date, inSameDayAs: date)
    }
    
    func getDate() -> [String] {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM YYYY"
        let date = formatter.string(from: currentDate)
        return date.components(separatedBy: " ")
    }
    
    func getCurrentMonth() -> Date {
        let calendar = Calendar.current
        // Get current month
        guard let currentMonth = calendar.date(byAdding: .month, value: self.currentMonth, to: Date()) else { return Date() }
        return currentMonth
    }
    
    func getCurrentMonthName() -> String {
        let now = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "LLLL"
        let nameOfMonth = dateFormatter.string(from: now)
        return nameOfMonth
    }
    
    func getDayOfTheWeek(date: Date) -> String {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        let dayInWeek = dateFormatter.string(from: date)
        return dayInWeek
    }
    
    func checkIsClosed(date: Date) -> Bool {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        let dayInWeek = dateFormatter.string(from: date)
        switch dayInWeek {
        case "domenica", "lunedì":
            return true
        default:
            return false
        }
    }
    
    // Extract date
    func extractDate() -> [DateValue] {
        let calendar = Calendar.current
        // Get current month
        let currentMonth = getCurrentMonth()
        var days = currentMonth.getAllDates().compactMap { date -> DateValue in
            // Get day
            let day = calendar.component(.day, from: date)
            let weekDay = calendar.component(.weekday, from: date)
            return DateValue(day: day, date: date, weekDay: weekDay)
        }
        let firstWeekDay = calendar.component(.weekday, from: days.first?.date ?? Date())
        for _ in 0..<firstWeekDay - 1 {
            days.insert(DateValue(day: -1, date: Date(), weekDay: 1), at: 0)
        }
        return days
    }
    
    // Extract date
    func getDays() -> [String] {
        var daysArray = [String]()
        let calendar = Calendar.current
        // Get current month
        let currentMonth = getCurrentMonth()
        var days = currentMonth.getAllDates().compactMap { date -> DateValue in
            // Get day
            let day = calendar.component(.day, from: date)
            let weekDay = calendar.component(.weekday, from: date)
            return DateValue(day: day, date: date, weekDay: weekDay)
        }
        let firstWeekDay = calendar.component(.weekday, from: days.first?.date ?? Date())
        for _ in 0..<firstWeekDay - 1 {
            days.insert(DateValue(day: -1, date: Date(), weekDay: 1), at: 0)
        }
        for index in 0..<days.count {
            daysArray.append("\(days[index].day)")
        }
        return daysArray
    }
    
    
}
