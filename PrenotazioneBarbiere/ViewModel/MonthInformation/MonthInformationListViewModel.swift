//
//  MonthInformationListViewModel.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 10/03/22.
//

import Foundation

class MonthInformationListViewModel: ObservableObject {
    private var repository: MonthInformationFirebaseRepository
    
    @Published var monthInformations: [MonthInformationViewModel] = []
    @Published var isClodingDay: Bool = false
    @Published var isLoading: Bool = false
    
    init() {
        repository = MonthInformationFirebaseRepository()
    }
    
    func getAllMonthInformation() {
        repository.getMonthInformation { result in
            switch result {
            case .success(let fetchedMonthInformations):
                if let fetchedMonthInformations = fetchedMonthInformations {
                    self.isLoading = true
                    DispatchQueue.main.async {
                        self.monthInformations = fetchedMonthInformations.map(MonthInformationViewModel.init)
                        self.isLoading = false
                    }
                }
            case .failure(let error):
                debugPrint("Error: \(error.localizedDescription)")
            }
        }
    }
}
