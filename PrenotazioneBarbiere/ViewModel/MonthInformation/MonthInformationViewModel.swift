//
//  MonthInformationViewModel.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 10/03/22.
//

import Foundation

struct MonthInformationViewModel {
    let monthInformation: MonthInformation
    
    var timetables: [String] {
        monthInformation.timetables
    }
    
    var name: String? {
        monthInformation.id ?? ""
    }
    
    var isClosingDay: Bool {
        monthInformation.isClosingDay
    }
}
