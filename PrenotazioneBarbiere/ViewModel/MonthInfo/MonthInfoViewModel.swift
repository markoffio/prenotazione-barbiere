//
//  MonthInfoViewModel.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 03/03/22.
//

import Foundation

struct MonthInfoViewModel {
    var monthInfo: MonthInfo
    var id: String { monthInfo.id ?? "" }
    var days: [Int] { monthInfo.days }
    var month: String { monthInfo.month }
}
