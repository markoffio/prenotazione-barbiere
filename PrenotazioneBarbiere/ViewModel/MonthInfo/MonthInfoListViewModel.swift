//
//  MonthInfoListViewModel.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 03/03/22.
//

import Foundation

final class MonthInfoListViewModel: ObservableObject, Identifiable {
    // MARK: - Properties
    var id = UUID()
    // Closed days per month repository protocol
    private var repository: MonthInfoRepositoryProtocol
    // Array of month info
    @Published var monthInfo = [MonthInfoViewModel]()
    @Published var closedDays = [Int]()
    // Is loading
    @Published var isLoading = false
    
    // MARK: - Functions
    
    init(repository: MonthInfoRepositoryProtocol) { self.repository = repository }
    
    // Fetch month info
    func fetchInfo() {
        repository.fetchAll { result in
            switch result {
            case .success(let info):
                if let info = info {
                    self.isLoading = true
                    DispatchQueue.main.async { [self] in
                        self.isLoading = false
                        self.monthInfo = info.map(MonthInfoViewModel.init)
                        guard let closedDays = self.monthInfo.first?.days else { return }
                        self.closedDays = closedDays
                    }
                }
            case .failure(let error):
                debugPrint("[ERROR]: \(error.localizedDescription)")
            }
        }
    }
}
