//
//  ServiceViewModel.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 10/11/21.
//

import Foundation

struct ServiceViewModel {
    var service: Service
    var id: String { service.id ?? "" }
    var name: String { service.name }
    var price: Int { service.price }
    var category: String { service.category }
}

