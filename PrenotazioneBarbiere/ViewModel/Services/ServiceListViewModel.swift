//
//  ServiceListViewModel.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 10/11/21.
//

import Foundation

final class ServiceListViewModel: ObservableObject, Identifiable {
    // MARK: - Properties
    var id = UUID()
    // Service repository protocol
    private var repository: ServiceRepositoryProtocol
    // Array of services
    @Published var services = [ServiceViewModel]()
    // Is loading
    @Published var isLoading = false
    
    // MARK: - Functions
    
    init(repository: ServiceRepositoryProtocol) { self.repository = repository }
    
    // Fetch all services
    func fetchAllServices() {
        repository.fetchAll { [self] result in
            switch result {
            case .success(let fetchedServices):
                if let fetchedServices = fetchedServices {
                    self.isLoading = true
                    DispatchQueue.main.async {
                        self.isLoading = false
                        self.services = fetchedServices.map(ServiceViewModel.init)
                    }
                }
            case .failure(let error):
                debugPrint("[ERROR]: \(error.localizedDescription)")
            }
        }
    }
}
