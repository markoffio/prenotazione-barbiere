//
//  NoNetworkConnectionViewModel.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 16/12/21.
//

import SwiftUI

final class NoNetworkConnectionViewModel: ObservableObject {
    
    func openNetworkSettings(){
        if let url = URL(string: "App-prefs:root=General&path=Network") {
            if UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
    }
    
}
