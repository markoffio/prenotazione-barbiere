//
//  AuthenticationViewModel.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 05/11/21.
//

import Firebase
import UIKit
import SwiftUI

class AuthenticationViewModel: NSObject, ObservableObject {
    // MARK: - Properties
    
    // Is registration view presented
    @Published var isRegistrationViewPresented: Bool = false
    // Is main view presented
    @Published var isMainViewPresented: Bool = false
    // Is forgot password view presented
    @Published var isForgotPasswordViewPresented: Bool = false
    // Is showing info alert
    @Published var isShowingInfoAlert: Bool = false
    // Used to check if the user is authenticated
    @Published var didAuthenticateCustomer: Bool = false
    // Is loading
    @Published var isLoading: Bool = false
    // Customer session
    @Published var customerSession: FirebaseAuth.User?
    // Current customer
    @Published var currentCustomer: Customer?
    // Alert item
    @Published var alertItem: AlertItem?
    // Fullname error
    @Published var fullNameError: String = ""
    // Email error
    @Published var emailError: String = ""
    // Phone number error
    @Published var phoneNumberError: String = ""
    // Password error
    @Published var passwordError: String = ""
    
    // Fullname
    @Published var fullName: String = "" {
        didSet {
            if self.fullName.isEmpty {
                self.fullNameError = AuthenticationMessage.fullNameError
            }
        }
    }
    
    // Email
    @Published var email: String = "" {
        didSet {
            if self.email.isEmpty {
                self.emailError = AuthenticationMessage.noEmailAddressError
            } else if !self.email.isEmailValid() {
                self.emailError = AuthenticationMessage.invalidEmailAddress
            } else {
                self.emailError = ""
            }
        }
    }
    
    // Password
    @Published var password: String = "" {
        didSet {
            if self.password.isEmpty {
                self.passwordError = AuthenticationMessage.noPasswordError
            } else if !self.password.isPasswordValid() {
                self.passwordError = AuthenticationMessage.invalidPasswordError
            }
            else {
                self.passwordError = ""
            }
        }
    }
    
    // Phone number
    @Published var phoneNumber: String = "" {
        didSet {
            if self.phoneNumber.isEmpty {
                self.phoneNumberError = AuthenticationMessage.noPhoneNumberError
            } else if !self.phoneNumber.isPhoneNumberValid() {
                self.phoneNumberError = AuthenticationMessage.invalidPhoneNumberError
            } else {
                self.phoneNumberError = ""
            }
        }
    }

    // Shared instance
    static let shared = AuthenticationViewModel()
    // Temporary current customer
    private var tempCurrentCustomer: FirebaseAuth.User?
    
    // Initializer
    override init() {
        super.init()
        // Initialize customer session
        customerSession = Auth.auth().currentUser
        fetchCustomer()
    }
    
    // MARK: - Functions
    
    // Check if the password is valid
    fileprivate func isValidPassword() {
        guard !self.password.isEmpty else {
            self.passwordError = AuthenticationMessage.noPasswordError
            return
        }
        let setPasswordError = self.password.isPasswordValid() == false
        if setPasswordError {
            if self.password.count < minimumPasswordLength {
                self.passwordError = AuthenticationMessage.invalidPasswordLength
                return
            }
            if !self.password.isUpperCase() {
                self.passwordError = AuthenticationMessage.passwordNoUppercaseCharacter
                return
            }
            if !self.password.isLowerCase() {
                self.passwordError = AuthenticationMessage.passwordNoLowercaseCharacter
                return
            }
            if !self.password.containsCharacters() {
                self.passwordError = AuthenticationMessage.passwordNoSpecialCharacter
                return
            }
            if !self.password.containsDigits() {
                self.passwordError = AuthenticationMessage.passowordNoNumber
                return
            }
        } else {
            self.passwordError = ""
        }
    }
    
    // Show forgot password view
    func showForgotPasswordView() {
        isForgotPasswordViewPresented.toggle()
    }
    
    // Show register view
    func showRegisterView() {
        isRegistrationViewPresented.toggle()
    }
    
    // Show main view
    func showMainView() {
        isMainViewPresented.toggle()
    }
    
    // Show info alert
    func showInfoAlert() {
        isShowingInfoAlert.toggle()
        alertItem = AlertContext.forgotPasswordInformation
    }
    
    // Login customer
    func login(withEmail email: String, password: String) {
        showLoadingView()
        Auth.auth().signIn(withEmail: email, password: password) { [self] authDataResult, error in
            // Checking for errors
            if let error = error {
                handleError(error)
                return
            }
            guard let customer = authDataResult?.user else { return }
            customerSession = customer
            fetchCustomer()
            hideLoadingView()
        }
    }
    
    // Register customer
    func register(withEmail email: String, password: String, fullName: String, phoneNumber: String) {
        showLoadingView()
        Auth.auth().createUser(withEmail: email, password: password) { [self] authDataResult, error in
            // Checking for errors
            if let error = error {
                handleError(error)
                return
            }
            guard let customer = authDataResult?.user else { return }
            customerSession = customer
            fetchCustomer()
            
            let data: [String: Any] = [FirebaseCollections.Customers.email: email, FirebaseCollections.Customers.fullName: fullName, FirebaseCollections.Customers.phoneNumber: phoneNumber]
            
            FirebaseCollections.Customers.collectionName.document(customer.uid).setData(data) { [self] _ in
                cleanTextFields()
                self.didAuthenticateCustomer = true
            }
            hideLoadingView()
        }
    }
    
    // Sign out
    func signout() {
        showLoadingView()
        self.customerSession = nil
        do {
            try Auth.auth().signOut()
            hideLoadingView()
        } catch let error {
            self.handleError(error)
        }
    }
    
    // Send password reset
    func passwordReset(withEmail email: String) {
        showLoadingView()
        Auth.auth().sendPasswordReset(withEmail: email) { [self] error in
            // Checking for errors
            if let error = error {
                hideLoadingView()
                handleError(error)
                return
            }
            hideLoadingView()
            alertItem = AlertContext.passwordResetInstructionsSent
        }
    }
    
    // Fetch customer
    fileprivate func fetchCustomer() {
        // Get current user id
        guard let uid = customerSession?.uid else { return }
        FirebaseCollections.Customers.collectionName.document(uid).getDocument { [self] snapshot, _ in
            guard let customer = try? snapshot?.data(as: Customer.self) else { return }
            self.currentCustomer = customer
        }
    }
    
    // Handle authentication error
    fileprivate func handleError(_ error: Error) {
        if let errorCode = AuthErrorCode(rawValue: error._code) {
            alertItem = AlertItem.init(title: Text("Errore"), message: Text(errorCode.errorMessage), dismissButton: .default(Text("OK")))
            hideLoadingView()
        }
    }
    
    // Show loading view
    private func showLoadingView() {
        isLoading = true
    }
    
    // Hide loading view
    private func hideLoadingView() {
        isLoading = false
    }
    
    // Clean all textfields
    func cleanTextFields() {
        fullName = ""
        email = ""
        phoneNumber = ""
        password = ""
    }
}
