//
//  TimeSlotsPerDayFirebaseServiceViewModel.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 07/03/22.
//
/*
import Foundation

final class TimeSlotsPerDayFirebaseServiceViewModel: ObservableObject, Identifiable {
    // MARK: - Properties
    var id = UUID()
    // Closed days per month repository protocol
    private var repository: TimeslotsPerDayRepositoryProtocol
    // Array of month info
    @Published var timeSlotsPerDay = [TimeSlotsPerDayViewModel]()
    //@Published var timeSlotsPerDay = TimeSlotsPerDayViewModel()
    // Is loading
    @Published var isLoading = false
    
    // MARK: - Functions
    
    init(repository: TimeslotsPerDayRepositoryProtocol) { self.repository = repository }
    
    // Fetch time slots
    func fetchTimeslots() {
        repository.fetchAll { [self] result in
            switch result {
            case .success(let info):
                if let info = info {
                    self.isLoading = true
                    DispatchQueue.main.async {
                        self.isLoading = false
                        timeSlotsPerDay = info.map(TimeSlotsPerDayViewModel.init)
                    }
                }
            case .failure(let error):
                debugPrint("[ERROR]: \(error.localizedDescription)")
            }
        }
    }
}
*/
