//
//  TimeSlotsViewModel.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 07/03/22.
//

import SwiftUI
import Firebase

class TimeSlotsViewModel: ObservableObject {
    @Published var timetables: [String] = []
    
    
    // Fetch time slot for selected day
    /*
    func fetchTimeSlots(day: String) {
        let query = FirebaseCollections.TimeslotsPerDay.collectionName
            .document("marzo-2022")
            .collection("timetables")
            .document(day)
        query.getDocument { document, error in
            if let document = document, document.exists {
                let timetables = document.get("timetables")
                self.timetables = timetables as! [String]
            } else {
                debugPrint("Document does not exist")
            }
        }
    }
    */
    
    fileprivate func fetchTimeSlots(month: String, timeTableCollection: String, day: String, completion: @escaping (Result<TimeSlot?, Error>) -> Void) {
        let query = FirebaseCollections.TimeslotsPerDay.collectionName
            .document(month)
            .collection(timeTableCollection)
            .document(day)
        query.getDocument { documentSnapshot, error in
            guard let documentSnapshot = documentSnapshot, error == nil else {
                completion(.failure(error ?? NSError(domain: "[snapshot is nil]", code: 102, userInfo: nil)))
                return
            }
            var timeSlot = try? documentSnapshot.data(as: TimeSlot.self)
            //self.timetables = timeSlot?.timetables
            if timeSlot != nil {
                //self.timetables = timeSlot?.timetables ?? ["9:30"]
                timeSlot!.id = documentSnapshot.documentID
            }
            //return timeSlot
            completion(.success(timeSlot))
        }
    }
    
    func fetchInfoForSelectedDay(month: String, timeTableCollection: String, day: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.fetchTimeSlots(month: month, timeTableCollection: timeTableCollection, day: day) { result in
                switch result {
                case .success(let fetchedTimeSlots):
                    DispatchQueue.main.async {
                        self.timetables = fetchedTimeSlots?.timetables ?? [""]
                    }
                case .failure(let error):
                    debugPrint("Error fetching timeslots: \(error.localizedDescription)")
                }
            }
        }
    }
    
    
    // Delete selected time slot
    func deleteTimeslot(day: String, selectedTimeSlot: String) {
        
    }
}
