//
//  CustomerReservationService.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 14/11/21.
//

import Firebase
import FirebaseFirestore
import FirebaseFirestoreSwift

struct CustomerReservationService {
    
    // Create customer reservation entry
    /*func createCustomerReservationEntry(customerReservation: CustomerReservation, completion: @escaping(_ error: Error?) -> Void) {
        do {
            let _ = try FirebaseCollections.Reservations.collectionName.document(customerReservation.selectedDate.toString).collection(customerReservation.customerId).addDocument(from: customerReservation)
            completion(nil)
        } catch let createCustomerReservationEntryError {
            completion(createCustomerReservationEntryError)
        }
    }*/
    
    func createCustomerReservationEntry(customerReservation: CustomerReservation, completion: @escaping(Result<CustomerReservation?, Error>) -> Void) {
        do {
            let _ = try FirebaseCollections.Reservations.collectionName.document(customerReservation.selectedDate.toString).collection(customerReservation.customerId).addDocument(from: customerReservation)
        } catch let createCustomerReservationEntryError {
            completion(.failure(createCustomerReservationEntryError))
        }
    }
}
