//
//  DateRoundedView.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 12/09/21.
//

import SwiftUI

enum TwelveHourClock: String {
    case am = "am"
    case pm = "pm"
}

struct DateRoundedView: View {
    // MARK: - Properties
    
    // Service selected
    @State private var selected: Bool = false
    // Index
    let index: Int
    // Hour
    var hour: String
        
    var body: some View {
        ZStack {
            RoundedRectangle(cornerRadius: 20)
                .strokeBorder(Color.lightBlack, lineWidth: 1)
                .background(RoundedRectangle(cornerRadius: 20).fill(selected ? Color.lightBlack : .clear))
                .frame(height: 30)
            Text(hour)
                .font(Font.body)
                .foregroundColor(selected ? .white : Color.lightBlack)
                .frame(width: 50)
                .padding()
        }
        .onTapGesture {
            selected.toggle()
        }
    }
}

/*
struct DateRoundedView_Previews: PreviewProvider {
    static var previews: some View {
        DateRoundedView(hour: "10:30")
    }
}
*/
