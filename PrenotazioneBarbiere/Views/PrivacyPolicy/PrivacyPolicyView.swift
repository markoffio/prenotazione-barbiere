//
//  PrivacyPolicyView.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 01/12/21.
//

import SwiftUI

struct PrivacyPolicyView: View {
    var body: some View {
        VStack {
            ScrollView(showsIndicators: false) {
                VStack(alignment: .leading) {
                    GroupBox {
                        Text("Questa Applicazione raccoglie alcuni Dati Personali dei propri Utenti.                    ")
                            .multilineTextAlignment(.leading)
                            .font(Font.info)
                            .foregroundColor(Color.gray)
                    } label: {
                        Text("Privacy Policy di Prenotazione Barbiere")
                            .multilineTextAlignment(.leading)
                            .font(Font.infoTitle)
                            .foregroundColor(Color.lightBlack)
                    }
                    .groupBoxStyle(CardGroupBoxStyle())
                    GroupBox {
                        Text("Prenotazione Barbiere s.r.l\nVia Garibaldi, 11 - 84070 Stella Cilento (Italy)\nVAT/C.F./P.IVA: -----------\nCamera di commercio\nCS: 12987 Eur (I.V.)")
                            .multilineTextAlignment(.leading)
                            .font(Font.info)
                            .foregroundColor(Color.gray)
                    } label: {
                        Text("Titolare del Trattamento dei Dati")
                            .multilineTextAlignment(.leading)
                            .font(Font.infoTitle)
                            .foregroundColor(Color.lightBlack)
                        Text("Indirizzo email del titolare: margarucci.marco@gmail.com                    ")
                            .multilineTextAlignment(.leading)
                            .font(Font.info)
                            .foregroundColor(Color.gray)
                    }
                    .groupBoxStyle(CardGroupBoxStyle())
                    GroupBox {
                        Text("Fra i Dati Personali raccolti da questa Applicazione, in modo autonomo o tramite terze parti, ci sono: Permesso Contatti; Permesso Camera; Permesso Localizzazione approssimativa (continua); Permesso Microfono; Permesso Account social media; Cookie; Dati di utilizzo; email; password; identificatori univoci di dispositivi per la pubblicità (Google Advertiser ID o identificatore IDFA, per esempio).")
                            .multilineTextAlignment(.leading)
                            .font(Font.info)
                            .foregroundColor(Color.gray)
                            .padding(.bottom, 2)
                        Text("Dettagli completi su ciascuna tipologia di dati raccolti sono forniti nelle sezioni dedicate di questa privacy policy o mediante specifici testi informativi visualizzati prima della raccolta dei dati stessi. I Dati Personali possono essere liberamente forniti dall'Utente o, nel caso di Dati di Utilizzo, raccolti automaticamente durante l'uso di questa Applicazione. Se non diversamente specificato, tutti i Dati richiesti da questa Applicazione sono obbligatori. Se l’Utente rifiuta di comunicarli, potrebbe essere impossibile per questa Applicazione fornire il Servizio. Nei casi in cui questa Applicazione indichi alcuni Dati come facoltativi, gli Utenti sono liberi di astenersi dal comunicare tali Dati, senza che ciò abbia alcuna conseguenza sulla disponibilità del Servizio o sulla sua operatività. Gli Utenti che dovessero avere dubbi su quali Dati siano obbligatori, sono incoraggiati a contattare il Titolare. L’eventuale utilizzo di Cookie - o di altri strumenti di tracciamento - da parte di questa Applicazione o dei titolari dei servizi terzi utilizzati da questa Applicazione, ove non diversamente precisato, ha la finalità di fornire il Servizio richiesto dall'Utente, oltre alle ulteriori finalità descritte nel presente documento e nella Cookie Policy, se disponibile.")
                            .multilineTextAlignment(.leading)
                            .font(Font.info)
                            .foregroundColor(Color.gray)
                    } label: {
                        Text("Tipologie di Dati raccolti")
                            .multilineTextAlignment(.leading)
                            .font(Font.infoTitle)
                            .foregroundColor(Color.lightBlack)
                    }
                    .groupBoxStyle(CardGroupBoxStyle())
                    GroupBox {
                        Text("Modalità di trattamento\nIl Titolare adotta le opportune misure di sicurezza volte ad impedire l’accesso, la divulgazione, la modifica o la distruzione non autorizzate dei Dati Personali. Il trattamento viene effettuato mediante strumenti informatici e/o telematici, con modalità organizzative e con logiche strettamente correlate alle finalità indicate. Oltre al Titolare, in alcuni casi, potrebbero avere accesso ai Dati altri soggetti coinvolti nell’organizzazione di questa Applicazione (personale amministrativo, commerciale, marketing, legali, amministratori di sistema) ovvero soggetti esterni (come fornitori di servizi tecnici terzi, corrieri postali, hosting provider, società informatiche, agenzie di comunicazione) nominati anche, se necessario, Responsabili del Trattamento da parte del Titolare. L’elenco aggiornato dei Responsabili potrà sempre essere richiesto al Titolare del Trattamento.")
                            .multilineTextAlignment(.leading)
                            .font(Font.info)
                            .foregroundColor(Color.gray)
                            .padding(.bottom, 2)
                        Text("Base giuridica del trattamento\n Il Titolare tratta Dati Personali relativi all’Utente in caso sussista una delle seguenti condizioni: l’Utente ha prestato il consenso per una o più finalità specifiche; Nota: in alcuni ordinamenti il Titolare può essere autorizzato a trattare Dati Personali senza che debba sussistere il consenso dell’Utente o un’altra delle basi giuridiche specificate di seguito, fino a quando l’Utente non si opponga (“opt-out”) a tale trattamento. Ciò non è tuttavia applicabile qualora il trattamento di Dati Personali sia regolato dalla legislazione europea in materia di protezione dei Dati Personali;\nil trattamento è necessario all'esecuzione di un contratto con l’Utente e/o all'esecuzione di misure precontrattuali;\nil trattamento è necessario per adempiere un obbligo legale al quale è soggetto il Titolare;\nil trattamento è necessario per l'esecuzione di un compito di interesse pubblico o per l'esercizio di pubblici poteri di cui è investito il Titolare;\nil trattamento è necessario per il perseguimento del legittimo interesse del Titolare o di terzi.\nÈ comunque sempre possibile richiedere al Titolare di chiarire la concreta base giuridica di ciascun trattamento ed in particolare di specificare se il trattamento sia basato sulla legge, previsto da un contratto o necessario per concludere un contratto.")
                            .multilineTextAlignment(.leading)
                            .font(Font.info)
                            .foregroundColor(Color.gray)
                            .padding(.bottom, 2)
                        Text("Dettagli completi su ciascuna tipologia di dati raccolti sono forniti nelle sezioni dedicate di questa privacy policy o mediante specifici testi informativi visualizzati prima della raccolta dei dati stessi. I Dati Personali possono essere liberamente forniti dall'Utente o, nel caso di Dati di Utilizzo, raccolti automaticamente durante l'uso di questa Applicazione. Se non diversamente specificato, tutti i Dati richiesti da questa Applicazione sono obbligatori. Se l’Utente rifiuta di comunicarli, potrebbe essere impossibile per questa Applicazione fornire il Servizio. Nei casi in cui questa Applicazione indichi alcuni Dati come facoltativi, gli Utenti sono liberi di astenersi dal comunicare tali Dati, senza che ciò abbia alcuna conseguenza sulla disponibilità del Servizio o sulla sua operatività. Gli Utenti che dovessero avere dubbi su quali Dati siano obbligatori, sono incoraggiati a contattare il Titolare. L’eventuale utilizzo di Cookie - o di altri strumenti di tracciamento - da parte di questa Applicazione o dei titolari dei servizi terzi utilizzati da questa Applicazione, ove non diversamente precisato, ha la finalità di fornire il Servizio richiesto dall'Utente, oltre alle ulteriori finalità descritte nel presente documento e nella Cookie Policy, se disponibile.")
                            .multilineTextAlignment(.leading)
                            .font(Font.info)
                            .foregroundColor(Color.gray)
                    } label: {
                        Text("Modalità e luogo del trattamento dei Dati raccolti")
                            .multilineTextAlignment(.leading)
                            .font(Font.infoTitle)
                            .foregroundColor(Color.lightBlack)
                    }
                    .groupBoxStyle(CardGroupBoxStyle())
                }
                .padding()
            }
        }
        .navigationTitle("Privacy policy")
    }
}

struct PrivacyPolicyView_Previews: PreviewProvider {
    static var previews: some View {
        PrivacyPolicyView()
    }
}
