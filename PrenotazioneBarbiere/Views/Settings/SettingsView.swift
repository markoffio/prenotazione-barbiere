//
//  SettingsView.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 12/09/21.
//

import SwiftUI

struct SettingsView: View {
    // MARK: - Properties
    
    // View model
    @EnvironmentObject var viewModel: AuthenticationViewModel
    // Animation offset
    @State private var offset: CGFloat = .zero
    
    var body: some View {
        NavigationView {
            ZStack {
                if viewModel.isLoading { LoadingView() }
                else {
                    VStack {
                        ScrollView(showsIndicators: false) {
                            // MARK: - SECTION 1: Prenotazione barbiere
                            GroupBox {
                                HStack(alignment: .center, spacing: 10) {
                                    ZStack {
                                        RoundedRectangle(cornerRadius: 12)
                                            .fill(.white.opacity(0.9))
                                            .frame(width: 80, height: 80)
                                        Image("mustache")
                                            .resizable()
                                            .scaledToFit()
                                            .frame(width: 50, height: 50)
                                    }
                                    Spacer()
                                    Text("Prenotazione barbiere è l'app che ti permette di prenotare dal tuo barbiere in modo semplice e veloce")
                                        .font(Font.info)
                                        .foregroundColor(Color.lightGray)
                                }
                            } label: {
                                SettingsLabelView(title: "Prenotazione barbiere", image: "dot.radiowaves.left.and.right")
                            }
                            .groupBoxStyle(CardGroupBoxStyle())
                            .padding()
                            // MARK: - SECTION 2: Profilo
                            GroupBox {
                                NavigationLink {
                                    CustomerReservationsLocalListView()
                                } label: {
                                    SettingsRowView(icon: "calendar", text: "Prenotazioni", color: Color.lightBlue)
                                }
                                NavigationLink {
                                    EditCustomerProfileView()
                                } label: {
                                    SettingsRowView(icon: "person.circle", text: "Il mio profilo", color: Color.lightBlue)
                                }
                                Button {
                                    viewModel.signout()
                                } label: {
                                    SettingsRowView(icon: "figure.walk", text: "Esci", color: Color.titleRed)
                                }
                            } label: {
                                SettingsLabelView(title: "Profilo", image: "person.fill")
                            }
                            .groupBoxStyle(CardGroupBoxStyle())
                            .padding()
                            // MARK: - SECTION 3: Impostazioni
                            GroupBox {
                                NavigationLink {
                                    PrivacyPolicyView()
                                } label: {
                                    SettingsRowView(icon: "folder.fill", text: "Privacy policy", color: Color.lightBlue)
                                }
                                NavigationLink {
                                    TermsAndConditionsView()
                                } label: {
                                    SettingsRowView(icon: "info.circle.fill", text: "Termini e condizioni", color: Color.lightBlue)
                                }
                                Link(destination: URL(string: "https://www.apple.com")!) {
                                    SettingsRowView(icon: "globe", text: "Website", color: Color.lightBlue)
                                }
                            } label: {
                                SettingsLabelView(title: "Impostazioni", image: "apps.iphone")
                            }
                            .groupBoxStyle(CardGroupBoxStyle())
                            .padding()
                        }
                    }
                }
            }
            .navigationTitle("Impostazioni")
            .toolbar {
                Menu {
                    Button {
                        debugPrint("Sconti")
                    } label: {
                        Text("Sconto del 25%")
                    }
                } label: {
                    ZStack {
                        Image(systemName: "bell")
                            .resizable()
                            .scaledToFit()
                            .foregroundColor(Color.lightBlue)
                            .frame(width: 18, height: 18)
                        BlinkingView()
                            .offset(x: 5, y: -8)
                    }
                }
            }
        }
        .accentColor(Color.lightBlack)
    }
}

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        SettingsView()
    }
}
