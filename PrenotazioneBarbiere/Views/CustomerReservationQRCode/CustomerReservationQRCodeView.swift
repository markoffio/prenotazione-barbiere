//
//  CustomerReservationQRCodeView.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 02/12/21.
//

import SwiftUI

struct CustomerReservationQRCodeView: View {
    // View model
    @StateObject var viewModel = CustomerReservationQRCodeViewModel()
    @StateObject var customerReservationsLocal = CustomerReservationDetailsLocalViewModel()

    @EnvironmentObject var customerReservationDetailsLocal: CustomerReservationDetailsLocal
    
    @State private var qrCodeImage: UIImage = UIImage()
    @State private var isAnimating: Bool = false
    @State private var mainViewIsPresented: Bool = false
    @Environment(\.dismiss) var dismiss
    
    var body: some View {
        NavigationView {
            VStack {
                Spacer()
                Text("""
                    La tua prenotazione è andata a buon fine.
                    Controlla le tue prenotazioni nella sezione Impostazioni > Prenotazioni.
                    \n
                    Di seguito trovi il codice QR della tua prenotazione.
                    Ricorda di mostrarlo all'entrata.
                    """)
                    .font(Font.body)
                    .foregroundColor(Color.lightBlack)
                    .multilineTextAlignment(.center)
                Spacer()
                Image(uiImage: qrCodeImage)
                    .interpolation(.none)
                    .resizable()
                    .scaledToFit()
                    .frame(width: 200, height: 200)
                    .blur(radius: isAnimating ? 0 : 10)
                    .opacity(isAnimating ? 1 : 0)
                    .scaleEffect(isAnimating ? 1 : 0.5)
                    .animation(.easeOut(duration: 0.5), value: isAnimating)
                Spacer()
                Button {
                    mainViewIsPresented = true
                } label: {
                    Text("AVANTI")
                        .font(Font.button)
                        .fontWeight(.semibold)
                        .foregroundColor(.white)
                        .frame(maxWidth: .infinity)
                }
                .buttonStyle(.borderedProminent)
                .buttonBorderShape(.capsule)
                .controlSize(.large)
                .tint(Color.lightBlue)
                .fullScreenCover(isPresented: $mainViewIsPresented, content: MainView.init)
                Spacer()
            }
            .opacity(isAnimating ? 1 : 0)
            .offset(y: isAnimating ? 0 : -40)
            .animation(.easeOut(duration: 1.5), value: isAnimating)
            .onAppear {
                isAnimating = true
                qrCodeImage = viewModel.generateQRCode(from: customerReservationDetailsLocal.qrCodeInputString)
                customerReservationsLocal.saveReservations(customerId: customerReservationDetailsLocal.customerId,
                                                           date: customerReservationDetailsLocal.date,
                                                           selectedDate: customerReservationDetailsLocal.selectedDate,
                                                           selectedServices: customerReservationDetailsLocal.selectedServices,
                                                           time: customerReservationDetailsLocal.time,
                                                           totalPrice: customerReservationDetailsLocal.totalPrice,
                                                           qrCodeInputString: customerReservationDetailsLocal.qrCodeInputString)
            }
            .navigationBarHidden(true)
            .padding(.horizontal, 20)
        }
    }
}
