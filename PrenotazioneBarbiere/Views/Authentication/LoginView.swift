//
//  LoginView.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 06/11/21.
//

import SwiftUI

struct LoginView: View {
    // MARK: - Properties

    // Animation offset
    @State private var offset: CGFloat = .zero
    // Email address
    @State private var email: String  = ""
    // Password
    @State private var password: String = ""
    // Used to show/hide the password in secure field
    @State private var isSecureField: Bool = true
    // View model
    @EnvironmentObject var viewModel: AuthenticationViewModel
    
    var body: some View {
        NavigationView {
            VStack {
                VStack(spacing: 25) {
                    // Email text field
                    CustomTextField(text: $email, imageName: "envelope", placeholder: "email", isSecureField: false)
                        .keyboardType(.emailAddress)
                        .autocapitalization(.none)
                        .disableAutocorrection(true)
                        .overlay(alignment: .trailing) {
                            ClearTextFieldButton(text: $email)
                        }
                    // Password text field
                    CustomTextField(text: $password, imageName: "key", placeholder: "password", isSecureField: isSecureField)
                        .overlay(alignment: .trailing) {
                            Image(systemName: isSecureField ? "eye.slash" : "eye")
                                .resizable()
                                .scaledToFit()
                                .frame(width: 20, height: 20)
                                .foregroundColor(Color.lightGray)
                                .onTapGesture { isSecureField.toggle() }
                        }
                    // Forgot password
                    HStack {
                        Spacer()
                        NavigationLink {
                            ForgotPasswordView().navigationBarHidden(true)
                        } label: {
                            Text("password dimenticata?")
                                .foregroundColor(.gray)
                                .font(Font.body)
                        }
                    }
                    // Login button
                    Button {
                        viewModel.login(withEmail: email, password: password)
                    } label: {
                        Text("LOGIN")
                            .font(Font.button)
                            .foregroundColor(.white)
                            .frame(maxWidth: .infinity)
                    }
                    .buttonStyle(.borderedProminent)
                    .buttonBorderShape(.capsule)
                    .controlSize(.large)
                    .tint(Color.lightBlue)
                    .shadow(radius: 10)
                    .alert(item: $viewModel.alertItem, content: { $0.alert })
                    .padding(.top, 10)
                    NavigationLink {
                        RegisterView().navigationBarHidden(true)
                    } label: {
                        HStack {
                            Text("Non hai un account?")
                                .foregroundColor(.gray)
                            Text("Registrati.")
                                .foregroundColor(Color.lightBlue)
                        }
                        .font(Font.body)
                    }
//                    LottieView(animationName: "mustache_animation")
//                        .frame(width: 50, height: 50)
//                        .animation(Animation.linear(duration: 1).delay(0.3), value: offset)
                    if viewModel.isLoading { LoadingView() }
                }
                .padding(.top, 50)
                .padding(.horizontal, 20)
                Spacer()
            }
            .navigationBarTitle("Login")
        }
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}
