//
//  RegisterView.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 06/11/21.
//

import SwiftUI

struct RegisterView: View {
    // MARK: - Properties

    // Presentation mode
    @Environment(\.presentationMode) var presentationMode
    // View model
    @EnvironmentObject var viewModel: AuthenticationViewModel
    // Used to show/hide the password in secure field
    @State private var isSecureField: Bool = true
    
    
    // Animation offset
    @State private var offset: CGFloat = .zero
    
    var body: some View {
        NavigationView {
            ScrollView(.vertical, showsIndicators: false) {
                VStack {
                    VStack(spacing: 25) {
                        // Full name text field and error text
                        VStack(alignment: .leading, spacing: 4) {
                            CustomTextField(text: $viewModel.fullName, imageName: "person.circle", placeholder: "nome e cognome", isSecureField: false)
                                .keyboardType(.asciiCapable)
                                .autocapitalization(.none)
                                .overlay(alignment: .trailing) {
                                    ClearTextFieldButton(text: $viewModel.fullName)
                                }
                                //.disableAutocorrection(true)
                            if (viewModel.fullNameError.count != 0) {
                                Text(viewModel.fullNameError)
                                    .font(Font.body)
                                    .foregroundColor(Color.titleRed)
                            }
                        }
                        // Email text field and email error text
                        VStack(alignment: .leading, spacing: 4) {
                            CustomTextField(text: $viewModel.email, imageName: "envelope", placeholder: "email", isSecureField: false)
                                .keyboardType(.emailAddress)
                                .autocapitalization(.none)
                                .overlay(alignment: .trailing) {
                                    ClearTextFieldButton(text: $viewModel.email)
                                }
                                //.disableAutocorrection(true)
                            if (viewModel.emailError.count != 0) {
                                Text(viewModel.emailError)
                                    .font(Font.body)
                                    .foregroundColor(Color.titleRed)
                            }
                        }
                        // Phone number text field and phone number error text
                        VStack(alignment: .leading, spacing: 4) {
                            CustomTextField(text: $viewModel.phoneNumber, imageName: "phone", placeholder: "numero di telefono", isSecureField: false)
                                .keyboardType(.phonePad)
                                .autocapitalization(.none)
                                .disableAutocorrection(true)
                                .overlay(alignment: .trailing) {
                                    ClearTextFieldButton(text: $viewModel.phoneNumber)
                                }
                            if (viewModel.phoneNumberError.count != 0) {
                                Text(viewModel.phoneNumberError)
                                    .font(Font.body)
                                    .foregroundColor(Color.titleRed)
                            }
                        }
                        // Password text field and password error text
                        VStack(alignment: .leading, spacing: 4) {
                            CustomTextField(text: $viewModel.password, imageName: "key", placeholder: "password", isSecureField: isSecureField)
                                .overlay(alignment: .trailing) {
                                    Image(systemName: isSecureField ? "eye.slash" : "eye")
                                        .resizable()
                                        .scaledToFit()
                                        .frame(width: 20, height: 20)
                                        .foregroundColor(Color.lightGray)
                                        .onTapGesture { isSecureField.toggle() }
                                }
                            if (viewModel.passwordError.count != 0) {
                                Text(viewModel.passwordError)
                                    .font(Font.body)
                                    .foregroundColor(Color.titleRed)
                            }
                        }
                        // Register button
                        Button {
                            viewModel.register(withEmail: viewModel.email, password: viewModel.password, fullName: viewModel.fullName, phoneNumber: viewModel.phoneNumber)
                        } label: {
                            Text("CREA ACCOUNT")
                                .font(Font.button)
                                .foregroundColor(.white)
                                .frame(maxWidth: .infinity)
                        }
                        .buttonStyle(.borderedProminent)
                        .buttonBorderShape(.capsule)
                        .controlSize(.large)
                        .tint(Color.lightBlue)
                        .shadow(radius: 10)
                        .alert(item: $viewModel.alertItem, content: { $0.alert })
                        .padding(.top, 10)
                    }
                    Button {
                        presentationMode.wrappedValue.dismiss()
                    } label: {
                        HStack {
                            Text("Hai già un account?")
                                .foregroundColor(.gray)
                            Text("Effettua il login.")
                                .foregroundColor(Color.lightBlue)
                        }
                        .font(Font.body)
                    }
                    .padding(.top, 20)
                    if viewModel.isLoading { LoadingView().padding(.top, 50) }
//                    LottieView(animationName: "mustache_animation")
//                        .frame(width: 50, height: 50)
//                        .animation(Animation.linear(duration: 1).delay(0.3), value: offset)
                }
                .padding(.top, 50)
                .padding(.horizontal, 20)
                Spacer()
                if viewModel.customerSession != nil { MainView() }
            }
            .navigationBarTitle("Registrati")
        }
    }
    
    
}

struct RegisterView_Previews: PreviewProvider {
    static var previews: some View {
        RegisterView()
    }
}
