//
//  ForgotPasswordView.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 06/11/21.
//

import SwiftUI

struct ForgotPasswordView: View {
    // MARK: - Properties
    
    // Email
    @State private var email: String = ""
    // Presentation mode
    @Environment(\.presentationMode) var presentationMode
    // View model
    @EnvironmentObject var viewModel: AuthenticationViewModel
    // Animation offset
    @State private var offset: CGFloat = .zero
    
    var body: some View {
        NavigationView {
            VStack(spacing: 20) {
                Text("Inserisci l'indirizzo email fornito in fase di registrazione: riceverai via email le informazioni per modificare la password.")
                    .font(Font.body)
                    .foregroundColor(.gray)
                // Email text field
                CustomTextField(text: $email, imageName: "envelope", placeholder: "indirizzo email", isSecureField: false)
                    .keyboardType(.default)
                    .autocapitalization(.none)
                    .disableAutocorrection(true)
                    .padding(.top, 10)
                    .overlay(alignment: .trailing) {
                        ClearTextFieldButton(text: $email)
                    }
                // Send password reset information button
                Button {
                    viewModel.passwordReset(withEmail: email)
                } label: {
                    Text("RECUPERA PASSWORD")
                        .font(Font.button)
                        .foregroundColor(.white)
                        .frame(maxWidth: .infinity)
                }
                .buttonStyle(.borderedProminent)
                .buttonBorderShape(.capsule)
                .controlSize(.large)
                .tint(Color.lightBlue)
                .shadow(radius: 10)
                .alert(item: $viewModel.alertItem, content: { $0.alert })
                .padding(.top, 10)
                LottieView(animationName: "mustache_animation")
                    .frame(width: 50, height: 50)
                    .animation(Animation.linear(duration: 1).delay(0.3), value: offset)
                Spacer()
                if viewModel.isLoading { LoadingView() }
            }
            .navigationTitle("Recupero password")
            .toolbar(content: {
                ToolbarItem(placement: .navigationBarLeading) {
                    Button {
                        presentationMode.wrappedValue.dismiss()
                    } label: {
                        Image(systemName: "arrow.backward")
                            .foregroundColor(Color.lightBlue)
                    }
                }
                ToolbarItem(placement: .navigationBarTrailing) {
                    Button {
                        viewModel.showInfoAlert()
                    } label: {
                        Image(systemName: "info.circle")
                            .foregroundColor(.gray)
                    }
                }
            })
            .padding()
        }
    }
}

struct ForgotPasswordView_Previews: PreviewProvider {
    static var previews: some View {
        ForgotPasswordView()
    }
}
