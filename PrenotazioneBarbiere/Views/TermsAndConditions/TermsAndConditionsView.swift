//
//  TermsAndConditionsView.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 01/12/21.
//

import SwiftUI

struct TermsAndConditionsView: View {
    var body: some View {
        VStack {
            Text("Questa sezione è da completare...")
                .font(Font.body)
                .foregroundColor(Color.titleRed)
        }
        .navigationTitle("Termini e condizioni")
    }
}

struct TermsAndConditionsView_Previews: PreviewProvider {
    static var previews: some View {
        TermsAndConditionsView()
    }
}
