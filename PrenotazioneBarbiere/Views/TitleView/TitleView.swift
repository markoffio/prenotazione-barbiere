//
//  TitleView.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 07/11/21.
//

import Foundation
import SwiftUI

struct TitleView: View {
    // MARK: - Properties
    
    // Title
    let title: String
    
    var body: some View {
        VStack(alignment: .leading, spacing: 10) {
            HStack { Spacer() }
            Text(title)
                .font(Font.title)
                .foregroundColor(Color.lightBlue)
        }
    }
}

struct TitleView_Previews: PreviewProvider {
    static var previews: some View {
        TitleView(title: "Login")
    }
}
