//
//  EditCustomerProfileView.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 01/12/21.
//

import SwiftUI

struct EditCustomerProfileView: View {
    // Email address
    @State private var email: String  = ""
    @State private var isValid: Bool = false
    
    // View model
    @EnvironmentObject var viewModel: AuthenticationViewModel
    @ObservedObject var phoneNumber = NumbersOnly()
    
    var body: some View {
        VStack(spacing: 25) {
            HStack {
                Text("In questa sezione puoi modificare l'indirizzo email ed il tuo numero di telefono.")
                    .font(Font.body)
                    .foregroundColor(Color.lightGray)
                Spacer(minLength: 0)
            }
            // Email text field
            CustomTextField(text: $email, imageName: "envelope", placeholder: "email", isSecureField: false)
                .keyboardType(.emailAddress)
                .autocapitalization(.none)
                .disableAutocorrection(true)
            // Phone number textfield
            CustomTextField(text: $phoneNumber.value, imageName: "phone", placeholder: "numero di telefono", isSecureField: false)
                .keyboardType(.phonePad)
                .autocapitalization(.none)
                .disableAutocorrection(true)
            // Save button
            Button {
                isValid = !isPhoneNumberValid(phoneNumber.value)
                phoneNumber.value = ""
            } label: {
                Text("SALVA")
                    .font(Font.button)
                    .fontWeight(.semibold)
                    .foregroundColor(.white)
                    .frame(maxWidth: .infinity)
            }
            // TODO: Check if textfiels are empty
            .alert("Numbero di telefono non valido.", isPresented: $isValid) {
                Button("OK", role: .cancel) { }
            }
            .buttonStyle(.borderedProminent)
            .buttonBorderShape(.capsule)
            .controlSize(.large)
            .padding(.top, 10)
            .tint(Color.lightBlue)
            .shadow(radius: 10)
            Spacer()
        }
        .onAppear {
            guard let customerEmailAddress = viewModel.currentCustomer?.email else { return }
            guard let customerPhoneNumber = viewModel.currentCustomer?.phoneNumber else { return }
            email = customerEmailAddress
            phoneNumber.value = customerPhoneNumber
        }
        .navigationTitle("Modifica profilo")
        .padding(.top, 20)
        .padding(.horizontal, 20)
    }
    
    // MARK: - Functions
    // TODO: - Export to View Model
    
    // Check if the phone number is valid
    func isPhoneNumberValid(_ phoneNumber: String) -> Bool {
        let phoneRegex = "^\\s*(?:\\+?(\\d{1,3}))?[-. (]*(\\d{3})[-. )]*(\\d{3})[-. ]*(\\d{4})(?: *x(\\d+))?\\s*$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
        return phoneTest.evaluate(with: phoneNumber)
    }
}

struct EditCustomerProfileView_Previews: PreviewProvider {
    static var previews: some View {
        EditCustomerProfileView()
    }
}

// TODO: - Export to View Model
class NumbersOnly: ObservableObject {
    @Published var value = "" {
        didSet {
            let filtered = value.filter { $0.isNumber }
            
            if value != filtered {
                value = filtered
            }
        }
    }
}
