//
//  ServiceDetailsView.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 02/12/21.
//

import SwiftUI

struct ServiceDetailsView: View {
    
    @Environment(\.dismiss) var dismiss
    
    // Service name
    var serviceName: String
    // Service price
    var servicePrice: Int
    // Is favorite
    @State private var isFavorite: Bool = false
    
    var body: some View {
        VStack {
            VStack {
                //Spacer()
                Image("haircut_placeholder")
                    .resizable()
                    .opacity(0.8)
                    .scaledToFill()
                    .frame(width: 250, height: 250)
                    .padding(.top, 50)
                    .padding(.leading, 20)
                Spacer()
                ZStack {
                    RoundedRectangle(cornerRadius: 20)
                        .fill(.white)
                        .edgesIgnoringSafeArea([.leading, .trailing, .bottom])
                        .frame(height: 230)
                        .shadow(radius: 4)
                    VStack(alignment: .leading, spacing: 5) {
                        Text(serviceName)
                            .font(Font.title)
                            .bold()
                        ZStack {
                            RoundedRectangle(cornerRadius: 8)
                                .fill(Color.lightBlack)
                                .frame(width: 45, height: 30)
                            Text(String(format: "%d", servicePrice) + " €")
                                .font(Font.body)
                                .foregroundColor(.white)
                                .padding(.all, 6)
                        }
                        Text("Inserire una descrizione Inserire una descrizione  Inserire una descrizione  Inserire una descrizione  Inserire una descrizione  Inserire una descrizione  Inserire una descrizione  Inserire una descrizione ")
                            .font(Font.serviceDescription)
                            .foregroundColor(Color.lightBlack)
                            .padding(.top, 10)
                    }
                    .padding([.leading, .trailing, .bottom], 20)
                    .padding(.top, 5)
                }
            }
        }
        .navigationBarBackButtonHidden(true)
        .toolbar {
            // Back button
            ToolbarItem(placement: .navigationBarLeading) {
                Button {
                    dismiss()
                } label: {
                    Image(systemName: "chevron.left")
                }
            }
            // Add to bookmarks
            ToolbarItem(placement: .navigationBarTrailing) {
                Button {
                    isFavorite.toggle()
                } label: {
                    Image(systemName: isFavorite ? "heart.fill" : "heart")
                        .foregroundColor(Color.titleRed)
                }
            }
        }
    }
}
