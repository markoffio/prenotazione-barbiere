//
//  NoReservationsView.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 09/07/22.
//

import SwiftUI

struct NoReservationsView: View {
    
    var body: some View {
        VStack {
            ZStack {
                LottieView(animationName: "no_reservations")
                    .frame(height: 200.0)
                VStack {
                    Text("Non ci sono prenotazioni")
                        .font(Font.body)
                        .foregroundColor(Color.lightGray)
                    Text("Per effettuare una prenotazione")
                        .font(Font.info)
                        .foregroundColor(Color.lightGray)
                        .padding(.top, 5)
                    Text("accedi alla sezione \"Prenota\"")
                        .font(Font.info)
                        .foregroundColor(Color.lightGray)
                }
                .padding(.top, 300)
            }
            
    }
}
}
