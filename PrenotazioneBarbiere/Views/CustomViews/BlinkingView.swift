//
//  BlinkingView.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 10/12/21.
//

import SwiftUI

struct BlinkingView: View {
    
    @State private var isAnimating: Bool = false
    
    var body: some View {
        ZStack {
            Circle()
                .stroke(Color.titleRed.opacity(0.5), lineWidth: 2)
                .frame(width: 8, height: 8, alignment: .center)
                .background(Circle().fill(Color.titleRed))
            Circle()
                .stroke(Color.titleRed.opacity(0.5), lineWidth: 2)
                .frame(width: 8, height: 8, alignment: .center)
        }
        .blur(radius: isAnimating ? 0 : 10)
        .opacity(isAnimating ? 1 : 0)
        .scaleEffect(isAnimating ? 1 : 0.5)
        .animation(.easeOut(duration: 1).repeatForever(autoreverses: true), value: isAnimating)
        .onAppear {
            isAnimating = true
        }
    }
}

struct BlinkingView_Previews: PreviewProvider {
    static var previews: some View {
        BlinkingView()
    }
}
