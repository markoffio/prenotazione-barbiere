//
//  NoNetworkConnectionView.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 29/11/21.
//

import SwiftUI

struct NoNetworkConnectionView: View {
    
    @StateObject var viewModel = NoNetworkConnectionViewModel()
    
    var body: some View {
        VStack {
            ZStack {
                LottieView(animationName: "no_connection")
                Text("Non sei connesso ad internet")
                    .font(Font.body)
                    .foregroundColor(Color.titleRed)
                    .padding(.top, 240)
            }
            Button {
                viewModel.openNetworkSettings()
            } label: {
                Text("Impostazioni")
                    .padding()
                    .font(Font.button)
                    .foregroundColor(Color(.white))
            }
            .buttonStyle(.borderedProminent)
            .buttonBorderShape(.capsule)
            .controlSize(.small)
            .tint(Color.lightBlue)
            .shadow(radius: 10)
            .padding(.bottom, 60)
        }
    }
}
