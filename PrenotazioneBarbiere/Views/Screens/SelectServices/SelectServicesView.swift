//
//  SelectServicesView.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 11/11/21.
//

import SwiftUI

struct SelectServicesView: View {
    // MARK: - Properties
    
    // Repository
    private var repository: ServiceRepositoryProtocol
    // View model
    @ObservedObject var serviceListViewModel: ServiceListViewModel
        
    @State private var isReservationDetailViewPresented: Bool = false
    @State private var search: String = ""
    // Service selected
    @State private var selected: Bool = false
    // Selected services
    @State private var selectedServices = [[String: Int]]()
    // Reservation details
    @EnvironmentObject var reservationDetails: ReservationDetails
        
    init(repository: ServiceRepositoryProtocol) {
        self.repository = repository
        serviceListViewModel = ServiceListViewModel(repository: repository)
    }
    
    var body: some View {
        VStack {
            ScrollView(.vertical, showsIndicators: false) {
                ForEach(serviceListViewModel.services, id: \.id) { service in
                    HStack {
                        Text(service.name)
                            .font(Font.body)
                            .foregroundColor(.black)
                        Spacer()
                        ZStack {
                            if selectedServices.contains([service.name: service.price]) {
                                Image(systemName: "circle.fill")
                                    .resizable()
                                    .frame(width: 16, height: 16)
                                    .foregroundColor(Color.lightBlue)
                            } else {
                                Image(systemName: "circle")
                                    .resizable()
                                    .frame(width: 16, height: 16)
                                    .foregroundColor(Color.lightBlue)
                            }
                        }
                    }
                    .contentShape(Rectangle())
                    .onTapGesture {
                        if selectedServices.contains([service.name: service.price]) {
                            selectedServices = selectedServices.filter { $0 != [service.name: service.price] }
                        } else {
                            selectedServices.append([service.name: service.price])
                        }
                    }
                    .padding(.vertical)
                }
            }
            .padding()
            // NEXT button
            HStack {
                Spacer()
                Button {
                    reservationDetails.selectedServices.removeAll()
                    reservationDetails.selectedServices.append(contentsOf: selectedServices)
                    reservationDetails.selectedServices = Array(Set(reservationDetails.selectedServices))
                    isReservationDetailViewPresented.toggle()
                } label: {
                    RoundedView(text: "AVANTI", backgroundColor: Color.lightBlue, foregroundColor: .white)
                        .shadow(radius: 10)
                }
                .sheet(isPresented: $isReservationDetailViewPresented, onDismiss: didDismiss, content: { ReservationDetailView().environmentObject(reservationDetails) })
                .opacity(selectedServices.count > 0 ? 1 : 0)
                .padding()
            }
        }
        .onAppear {
            serviceListViewModel.fetchAllServices()
        }
        .navigationBarTitle("Scegli i servizi", displayMode: .inline)
    }
    
    func didDismiss() {
        serviceListViewModel.fetchAllServices()
    }
}
