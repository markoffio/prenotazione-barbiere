//
//  SelectableServiceRowView.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 11/11/21.
//

import SwiftUI

struct SelectableServiceRowView: View {
    // MARK: - Properties
    
    // View model
    let viewModel: ServiceViewModel
    // Service selected
    @State private var selected: Bool = false
    // Selected services
    //@State private var selectedServices = [String: Int]()
    @State private var selectedServices: [String] = [String]()

    // Service name
    let name: String
    
    @EnvironmentObject var reservationDetails: ReservationDetails
                
    var body: some View {
        HStack {
            Text(viewModel.name)
                .font(Font.body)
                .foregroundColor(.black)
            Spacer()
            
            ZStack {
                if selectedServices.contains(viewModel.name) {
                    Image(systemName: "circle.fill")
                        .resizable()
                        .frame(width: 16, height: 16)
                        .foregroundColor(Color.lightBlue)
                } else {
                    Image(systemName: "circle")
                        .resizable()
                        .frame(width: 16, height: 16)
                        .foregroundColor(Color.lightBlue)
                }
            }
            .onTapGesture {
                if selectedServices.contains(viewModel.name) {
                    selectedServices = selectedServices.filter { $0 != viewModel.name }
                    //reservationDetails.selectedServices = reservationDetails.selectedServices.filter { $0 != [viewModel.name] }
                    //debugPrint(reservationDetails.selectedServices)
                    //reservationDetails.selectedServices.insert([service.name : service.price], at: 0)
                } else {
                    selectedServices.append(viewModel.name)
                    //reservationDetails.selectedServices.insert([service.name : service.price], at: 0)
                    //reservationDetails.selectedServices.append([viewModel.name])
                }
                debugPrint(selectedServices)
            }
        }
        .padding(.vertical)
        //.environmentObject(ReservationDetails())
    }
}
