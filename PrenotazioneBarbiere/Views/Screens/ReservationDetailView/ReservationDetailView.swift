//
//  ReservationDetailView.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 09/10/21.
//

import SwiftUI

struct ReservationDetailView: View {
    // MARK: - Properties
    
    // Animation offset
    @State private var offset: CGFloat = .zero
    
    @Environment(\.presentationMode) var presentationMode
    
    @EnvironmentObject var reservationDetails: ReservationDetails
    
    @StateObject private var customerReservationViewModel = CustomerReservationViewModel()
    @StateObject private var customerReservationDetailsLocal = CustomerReservationDetailsLocal()
    
    @State private var totalPrice: Int = 0
    @State private var selectedDate: Date = Date()
    @State private var time: String = ""
    @State private var selectedServices = [[String: Int]]()
    @State private var customerReservationViewIsPresented: Bool = false
    @State private var qrCodeInputString: String = ""

    var body: some View {
        VStack {
            HStack {
                Text("Riepilogo prenotazione")
                    .font(Font.subTitle)
                    .foregroundColor(Color.lightBlack)
                    .fontWeight(.semibold)
                Spacer()
                Button {
                    presentationMode.wrappedValue.dismiss()
                } label: {
                    Image(systemName: "xmark.circle.fill")
                        .resizable()
                        .frame(width: 20, height: 20)
                        .foregroundColor(Color.lightBlack)
                }
            }
            .padding([.vertical, .horizontal])
            Spacer()
                .frame(height: 50)
            // Card
            ZStack {
                RoundedRectangle(cornerRadius: 20)
                    .fill(Color.white)
                    .frame(height: 500)
                    .frame(maxWidth: .infinity)
                    .shadow(radius: 2)
                    .padding(.horizontal)
                VStack(alignment: .leading) {
                    VStack(alignment: .leading, spacing: 5) {
                        Text("Data prenotazione")
                            .foregroundColor(.gray)
                        Text("\(reservationDetails.selectedDate.toString)")
                            .foregroundColor(Color.lightBlack)
                        Text("Ora")
                            .foregroundColor(.gray)
                        Text("\(reservationDetails.time)")
                            .foregroundColor(Color.lightBlack)
                        Text("Lista servizi")
                            .foregroundColor(.gray)
                        ScrollView(.vertical, showsIndicators: false) {
                            ForEach(reservationDetails.selectedServices, id: \.self) { services in
                                let names = services.map { $0.key }
                                let prices = services.map { $0.value }
                                ForEach(names, id: \.self) { name in
                                    ForEach(prices, id: \.self) { price in
                                        HStack {
                                            Text(name)
                                                .foregroundColor(Color.lightBlack)
                                            Spacer()
                                            Text("\(price) €")
                                                .foregroundColor(Color.gray)
                                        }
                                        .padding(.top, 3)
                                    }
                                }
                            }
                        }
                        .frame(height: 300)
                        Divider()
                        HStack {
                            Spacer()
                            Text("Totale: \(totalPrice) €")
                                .foregroundColor(Color.lightBlack)
                                .padding(.top, 10)
                        }
                    }
                    .padding(.horizontal)
                }
                .font(Font.body)
                .padding(.horizontal)
            }
            Spacer()
            LottieView(animationName: "mustache_animation")
                .frame(width: 50, height: 50)
                .animation(Animation.linear(duration: 1).delay(0.3), value: offset)
                .padding(.top, -5)
            Spacer()
            // Make reservation button
            Button {
                let customerReservation = CustomerReservation(id: UUID().uuidString, customerFullName: AuthenticationViewModel.shared.currentCustomer!.fullName, customerId: AuthenticationViewModel.shared.currentCustomer!.id ?? "", email: AuthenticationViewModel.shared.currentCustomer!.email, date: Date(), selectedDate: reservationDetails.selectedDate, time: reservationDetails.time, selectedServices: reservationDetails.selectedServices, totalPrice: totalPrice)
                customerReservationViewModel.setCustomerReservationDetail(customerReservation: customerReservation)
                customerReservationDetailsLocal.customerId = AuthenticationViewModel.shared.currentCustomer!.id ?? ""
                customerReservationDetailsLocal.date = Date()
                customerReservationDetailsLocal.selectedDate = reservationDetails.selectedDate
                customerReservationDetailsLocal.time = reservationDetails.time
                customerReservationDetailsLocal.selectedServices = reservationDetails.selectedServices
                customerReservationDetailsLocal.totalPrice = totalPrice
                customerReservationDetailsLocal.qrCodeInputString = (AuthenticationViewModel.shared.currentCustomer!.id ?? "") + "-" + reservationDetails.selectedDate.toString
                customerReservationViewIsPresented.toggle()
            } label: {
                RoundedView(text: "PRENOTA", backgroundColor: Color.lightBlue, foregroundColor: .white)
                    .shadow(radius: 10)
            }
            .fullScreenCover(isPresented: $customerReservationViewIsPresented, content: CustomerReservationQRCodeView.init)
            .environmentObject(customerReservationDetailsLocal)
            .padding(.horizontal)
        }
        .onAppear {
            // TODO: - Remove
            //selectedServices.removeAll()
            reservationDetails.selectedServices = Array(Set(reservationDetails.selectedServices))
            // TODO: - Calculate price: extract to view model
            let prices = reservationDetails.selectedServices.compactMap { $0.values.reduce(0, +) }
            totalPrice = prices.reduce(0, { x, y in x + y })
        }
    }
}
