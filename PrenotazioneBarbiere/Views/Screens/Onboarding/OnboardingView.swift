//
//  OnboardingView.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 09/10/21.
//

import SwiftUI

struct OnboardingView: View {
    var body: some View {
        VStack {
            Text("BARBER SHOP")
                .font(Font.title)
                .foregroundColor(.lightBlack)
                .padding(.bottom, 30)
            Image(systemName: "mustache.fill")
                .resizable()
                .scaledToFit()
                .foregroundColor(.lightBlack)
                .frame(width: 80)
                .padding(.bottom, 50)
            VStack(alignment: .leading) {
                Text("Prenotazione")
                    .font(Font.subTitle)
                    .bold()
                    .foregroundColor(.lightBlack)
                    .padding(.bottom, 2)
                Text("E' possibile effettuare una prenotazione in pochi semplici passi.")
                    .font(Font.body)
                    .foregroundColor(.gray)
            }
                        Button(action: {}) {
                            RoundedView(text: "PRENOTA", backgroundColor: Color.lightBlue, foregroundColor: .white)
                                .shadow(radius: 10)
                        }
        }
        .padding()
    }
}

struct OnboardingView_Previews: PreviewProvider {
    static var previews: some View {
        OnboardingView()
    }
}
