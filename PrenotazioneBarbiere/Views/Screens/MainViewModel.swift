//
//  MainViewModel.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 09/10/21.
//

import Foundation

final class MainViewModel: NSObject, ObservableObject {
    // MARK: - Properties
    // Show time slots
    @Published var isShowingTimeSlots: Bool = false
}
