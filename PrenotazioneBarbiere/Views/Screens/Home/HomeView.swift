//
//  HomeView.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 12/09/21.
//

import SwiftUI

struct HomeView: View {
    // View model
    @StateObject private var viewModel = HomeViewModel()
    // Month
    let monthInt = Calendar.current.component(.month, from: Date())
    // Date
    @State private var date = Date()
    // Grid columns
    let columns = Array(repeating: GridItem(.flexible()), count: 7)
    
    @State private var openDays = [String]()
    // This is an array that contains days with no time slots available. For each single day we have n time slots available: this number will be decremented each time a user select
    // a time slot.
    let noMoreTimeSlotsAvailable = [22, 26]
    // @State private var noMoreTimeSlotsAvailable = [Int]()
    
    @State private var disable: Bool = false
    
    @State private var navigate: Bool = false
        
    @State private var selectedTime: String = String()
    @State private var selectedDate: Date = Date()
    @State private var isSelectedDate: Bool = false
    @State private var isSelectedTime: Bool = false
    @State private var isSelected: Bool = false
    @State private var selectedDay: Int = 0
    @State private var currentDay: Int = 0
    @State private var isClosingDay: Bool = false
    @State private var timetablesAvailable: Bool = false
    
    // Default time slots
    // TODO: - Read from Firebase
    //@State private var defaultTimeSlots = ["09:30", "10:30", "14:45", "17:40", "18:30", "20:00"]
    @State private var timetables: [String] = []
    @State var timetablesCount: Int = 0
    //@State private var timeSlots = ["09:30", "10:30", "14:45", "17:40", "18:30", "20:00"]
        
    //@EnvironmentObject var reservationDetails: ReservationDetails
    @StateObject private var reservationDetails = ReservationDetails()
    
    // Month info repository protocol
    private var monthInfoRepository: MonthInfoRepositoryProtocol
    // Month info view model
    @ObservedObject var monthInfoViewModel: MonthInfoListViewModel

    // Timeslots per day repository protocol
    private var timeslotsPerDayRepository: TimeslotsPerDayRepositoryProtocol
    // Timeslots per day view model
    //@ObservedObject var timeslotsPerDayViewModel: TimeSlotsPerDayFirebaseServiceViewModel
    
    @ObservedObject var timeSlotsViewModel = TimeSlotsViewModel()

    @ObservedObject private var monthInformation = MonthInformationListViewModel()
        
    init(monthInfoRepository: MonthInfoRepositoryProtocol, timeslotsPerDayRepository: TimeslotsPerDayRepositoryProtocol) {
        self.monthInfoRepository = monthInfoRepository
        self.timeslotsPerDayRepository = timeslotsPerDayRepository
        monthInfoViewModel = MonthInfoListViewModel(repository: monthInfoRepository)
        //timeslotsPerDayViewModel = TimeSlotsPerDayFirebaseServiceViewModel(repository: timeslotsPerDayRepository)
    }
            
    // Grid columns
    let gridColumns = [
        GridItem(.flexible()),
        GridItem(.flexible()),
        GridItem(.flexible())
    ]
    
    var monthString: String {
        Calendar.current.monthSymbols[monthInt-1]
    }
    
    var navigationTitle: String = HomeViewMessage.navigationTitle
        
    var body: some View {
        NavigationView {
            ZStack {
                if monthInformation.isLoading {
                    LoadingView()
                } else {
                    VStack(spacing: 10) {
                        // Custom date picker
                        ZStack {
                            RoundedRectangle(cornerRadius: 20)
                                .fill(.white)
                                .edgesIgnoringSafeArea([.leading, .trailing, .top])
                                .shadow(radius: 4)
                            VStack(spacing: 35) {
                                // Day view
                                HStack(spacing: 8) {
                                    ForEach(days, id: \.self) { day in
                                        Text(day)
                                            .font(Font.body)
                                            .bold()
                                            .frame(maxWidth: .infinity)
                                            .foregroundColor(day == "L" || day == "D" ? .titleRed : .lightBlue)
                                    }
                                }
                                .padding(.top, 20)
                                // Dates
                                LazyVGrid(columns: columns, spacing: 15) {
                                    ForEach(viewModel.extractDate()) { value in
                                        Button {
                                            monthInformation.getAllMonthInformation()
                                            selectedDate = value.date
                                            selectedDay = value.day
                                            reservationDetails.selectedDate = value.date
                                            isSelectedDate = true
                                            isSelected = true
                                            isSelectedTime = false
                                            
                                            // TODO: - Extract to view model
                                            guard let currentDayNumber = viewModel.getCurrentDayAsNumber() else { return }
                                                                                        
                                            let monthInformation = monthInformation.monthInformations.filter() { $0.name == String(selectedDay) }
                                            monthInformation.forEach { [self] element in
                                                timetables.removeAll()
                                                currentDayNumber == selectedDay ? (timetables = getSelectableTimeSlots(timetables: element.timetables)) : (timetables = element.timetables)
                                                timetablesCount = 0
                                                timetablesCount = timetables.count
                                                timetablesCount == 0 ? (timetablesAvailable = false) : (timetablesAvailable = true)
                                                isClosingDay = element.isClosingDay
                                            }
                                            // --- END TODO
                                            
                                        } label: {
                                            CardView(value: value)
                                        }
                                        .overlay {
                                            // Highlight selected day in the calendar
                                            if (isClosingDay || !timetablesAvailable) {
                                                RoundedRectangle(cornerRadius: 8, style: .continuous)
                                                    .fill(value.day == selectedDay ? Color.titleRed.opacity(0.4) : .clear)
                                                    .frame(width: 35, height: 35)
                                            } else {
                                                RoundedRectangle(cornerRadius: 8, style: .continuous)
                                                    .fill(value.day == selectedDay ? Color.lightBlue.opacity(0.4) : .clear)
                                                    .frame(width: 35, height: 35)
                                            }
                                        }
                                        .disabled(1...2 ~= value.weekDay) // Disable current button if selected day is sunday or monday
                                    }
                                }
                            }
                            .padding(.horizontal, 5)
                        }
                        if isSelected {
                            // Closing day
                            if isClosingDay {
                                VStack(spacing: 2) {
                                    Text(HomeViewMessage.closedMessage)
                                    Text(HomeViewMessage.selectDifferentDate)
                                }
                                .frame(maxHeight: .infinity)
                                .font(Font.body)
                                .foregroundColor(Color.titleRed)
                            } else {
                                // Check if the user can't book a reservation for selected day
                                if (!timetablesAvailable) {
                                    VStack(spacing: 2) {
                                        Text(HomeViewMessage.noTimeSlotsAvailable)
                                        Text(HomeViewMessage.selectDifferentDate)
                                    }
                                    .frame(maxHeight: .infinity)
                                    .font(Font.body)
                                    .foregroundColor(Color.titleRed)
                                } else {
                                    VStack(spacing: 2) {
                                        Text(HomeViewMessage.hourPerDay)
                                        Text(selectedDate.toString)
                                            .foregroundColor(Color.lightBlue)
                                        Timetables(items: timetables, selectedId: "", itemsCount: $timetablesCount, selected: $isSelectedTime) { selected in
                                            reservationDetails.time = selected
                                            isSelectedTime = true
                                        }
                                        // Select services button
                                        NavigationLink {
                                            SelectServicesView(repository: FirebaseServiceRepository()).environmentObject(reservationDetails)
                                        } label: {
                                            RoundedView(text: "SELEZIONA SERVIZIO", backgroundColor: Color.lightBlue, foregroundColor: .white)
                                                .shadow(radius: 10)
                                        }
                                        .opacity((isSelectedDate && isSelectedTime) ? 1 : 0)
                                        .padding(.horizontal)
                                    }
                                    .frame(maxHeight: .infinity)
                                    .font(Font.body)
                                }
                            }
                        } else {
                            VStack(spacing: 2) {
                                Text("Seleziona una data")
                                Text("per effettuare una prenotazione")
                            }
                            .frame(maxHeight: .infinity)
                            .font(Font.body)
                            .foregroundColor(Color.lightBlue)
                        }
                        Spacer()
                    }
                }
            }
            .navigationTitle(navigationTitle)
            .onAppear {
                monthInformation.getAllMonthInformation()
            }
        }
        .accentColor(Color.lightBlack)
    }
    
    @ViewBuilder
    func CardView(value: DateValue) -> some View {
        VStack(spacing: 2) {
            if value.day != -1 {
                Text("\(value.day)")
                    .font(Font.calendar)
                    .foregroundColor(((value.weekDay == 1 || value.weekDay == 2) ? .gray : (value.date.toString == Date.now.toString ? Color.lightBlue : .black)))
                    .frame(maxWidth: .infinity)
            }
        }
        .padding(.vertical, 5)
        .frame(height: 40, alignment: .center)
    }
    
    func checkIfNoMoreTimeSlotsAreAvailable(day: Int) -> Bool {
        return timetables.count > 0
    }
    
    func getSelectableTimeSlots(timetables: [String]) -> [String] {
        var integerTimeSlotArray = [Int]()
        let currentTimeString = Date().hour.replacingOccurrences(of: ":", with: "")
        // Convert default time slots array to an array of integers
        timetables.forEach { timeSlot in
            integerTimeSlotArray.append(Int(timeSlot.replacingOccurrences(of: ":", with: "")) ?? 0)
        }
        
        integerTimeSlotArray = integerTimeSlotArray.filter() { $0 > Int(currentTimeString) ?? 0 }.sorted { $0 < $1 }
                            
        var stringTimeSlotArray = [String]()
        integerTimeSlotArray.forEach { timeSlot in
            stringTimeSlotArray.append(String(timeSlot))
        }
        
        stringTimeSlotArray.forEach { timeSlot in
            var newTimeSlot = timeSlot
            if timeSlot.count == 4 {
                newTimeSlot.insert(":", at: newTimeSlot.index(newTimeSlot.startIndex, offsetBy: 2))
            } else {
                newTimeSlot.insert(":", at: newTimeSlot.index(newTimeSlot.startIndex, offsetBy: 1))
            }
            stringTimeSlotArray.removeFirst()
            stringTimeSlotArray.append(newTimeSlot)
        }
        return stringTimeSlotArray
    }
}

/*
struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
*/
