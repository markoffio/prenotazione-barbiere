//
//  ServicesListView.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 12/09/21.
//

import SwiftUI

enum Category: String {
    case capelli
    case viso
    case barba
}

struct ServicesListView: View {
    // MARK: - Properties
    
    // Animation offset
    @State private var offset: CGFloat = .zero
    // Navigation title
    @State var navigationTitle: String = servicesListViewNavigationTitle
    
    // Repository
    private var repository: ServiceRepositoryProtocol
    // View model
    @ObservedObject var serviceListViewModel: ServiceListViewModel
    
    init(repository: ServiceRepositoryProtocol) {
        self.repository = repository
        serviceListViewModel = ServiceListViewModel(repository: repository)
    }
    
    var body: some View {
        NavigationView {
            ZStack {
                if serviceListViewModel.isLoading {
                    LoadingView()
                } else {
                    VStack {
                        ScrollView(.vertical, showsIndicators: false) {
                            ForEach(serviceListViewModel.services, id: \.id) { service in
                                NavigationLink {
                                    ServiceDetailsView(serviceName: service.name, servicePrice: service.price)
                                } label: {
                                    ServiceRowView(viewModel: service)
                                }
                            }
                        }
                        .padding(.horizontal)
                    }
                }
            }
            .onAppear {
                serviceListViewModel.fetchAllServices()
            }
            .navigationTitle(navigationTitle)
            .animation(.default, value: offset)
        }
        .accentColor(Color.lightBlack)
    }
}
