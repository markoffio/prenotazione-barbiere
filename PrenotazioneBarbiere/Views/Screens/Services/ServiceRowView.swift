//
//  ServiceRowView.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 05/10/21.
//

import SwiftUI

struct ServiceRowView: View {
    // MARK: - Properties
    // View model
    let viewModel: ServiceViewModel
    
    var body: some View {
        HStack {
            // Service name
            Text(viewModel.name)
                .foregroundColor(Color.lightBlack)
            Spacer()
            // Service price
            Text(String(format: "%d", viewModel.price) + " €")
                .foregroundColor(.gray)
        }
        .font(Font.body)
        .frame(height: 50)
    }
}

/*
struct ServiceRowView_Previews: PreviewProvider {
    static var previews: some View {
        ServiceRowView(service: servicesData[0])
    }
}
*/
