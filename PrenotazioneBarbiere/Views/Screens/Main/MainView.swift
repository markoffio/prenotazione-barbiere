//
//  MainView.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 12/09/21.
//

import SwiftUI

struct MainView: View {
    var body: some View {
        TabView {
            HomeView(monthInfoRepository: FirebaseMonthInfoRepository(), timeslotsPerDayRepository: FirebaseTimeslotsPerDayRepository())
                .tabItem {
                    Label(reservationTitle, systemImage: reservationImage)
                        .foregroundColor(Color.lightBlack)
                }
                .environmentObject(ReservationDetails())
            ServicesListView(repository: FirebaseServiceRepository())
                .tabItem { Label(servicesTitle, systemImage: servicesImage) }
            SettingsView()
                .tabItem { Label(settingsTitle, systemImage: settingsImage)}
        }
        .accentColor(.lightBlue)
        .background(Color.background)
    }
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}
