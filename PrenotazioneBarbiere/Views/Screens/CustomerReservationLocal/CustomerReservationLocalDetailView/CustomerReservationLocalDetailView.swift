//
//  CustomerReservationLocalDetailView.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 03/12/21.
//

import SwiftUI

struct CustomerReservationLocalDetailView: View {
    
    var savedLocalReservation: CustomerReservationLocalEntity
    @StateObject var viewModel = CustomerReservationQRCodeViewModel()
    @State private var qrCodeImage: UIImage = UIImage()
    @State private var showConfirmationDialog = false
    
    var body: some View {
        VStack {
            // Customer reservation date header
            HStack {
                Text("\(savedLocalReservation.selectedDate!.toString) alle ore \(savedLocalReservation.time!)")
                    .font(Font.body)
                    .foregroundColor(Color.lightBlack)
                Spacer()
            }
            .padding(.horizontal)
            Spacer()
            // Reservation QR Code
            Image(uiImage: qrCodeImage)
                .interpolation(.none)
                .resizable()
                .scaledToFit()
                .frame(width: 200, height: 200)
                .padding(.top, 20)
                .padding(.bottom, 10)
            
            HStack {
                // TODO: - Inserire pagamento con Apple Pay
                Button {
                    debugPrint("INSERIRE PAGAMENTO CON APPLE PAY...")
                } label: {
                    Text("Paga \(savedLocalReservation.totalPrice) €")
                        .font(Font.button)
                        .foregroundColor(.white)
                }
                .buttonStyle(.borderedProminent)
                .buttonBorderShape(.capsule)
                .controlSize(.small)
                .tint(Color.lightBlue)
                /*
                // Delete current reservation
                Button {
                    showConfirmationDialog = true
                } label: {
                    Text("Elimina")
                        .font(Font.button)
                        .foregroundColor(.white)
                }
                .buttonStyle(.borderedProminent)
                .buttonBorderShape(.capsule)
                .controlSize(.small)
                .tint(Color.titleRed)
                .confirmationDialog("Vuoi cancellare questa prenotazione?", isPresented: $showConfirmationDialog) {
                    Button("Elimina prenotazione", role: .destructive) {
                        debugPrint("Delete current reservation")
                    }
                }*/
            }
            // Services list
            ZStack {
                RoundedRectangle(cornerRadius: 20)
                    .fill(.white)
                    .edgesIgnoringSafeArea([.leading, .trailing, .bottom])
                    .frame(maxHeight: .infinity)
                    .shadow(radius: 4)
                VStack(alignment: .leading) {
                    HStack {
                        Text("Lista servizi")
                            .font(Font.body)
                            .foregroundColor(.gray)
                    }
                    .padding(.bottom, 10)
                    Divider()
                        .padding(.bottom, 10)
                    ScrollView(.vertical, showsIndicators: false) {
                        ForEach(savedLocalReservation.selectedServices!, id: \.self) { services in
                            let names = services.map { $0.key }
                            let prices = services.map { $0.value }
                            ForEach(names, id: \.self) { name in
                                ForEach(prices, id: \.self) { price in
                                    HStack {
                                        Text(name)
                                            .foregroundColor(Color.lightBlack)
                                        Spacer()
                                        Text("\(price) €")
                                            .foregroundColor(Color.gray)
                                    }
                                    .font(Font.body)
                                    .padding(.top, 3)
                                }
                            }
                        }
                    }
                }
                .padding(.top, 20)
                .padding(.horizontal)
            }
            .padding(.top, 10)
        }
        .onAppear {
            qrCodeImage = viewModel.generateQRCode(from: savedLocalReservation.qrCodeInputString!)
        }
        .navigationTitle("Prenotazione del")
    }
}

/*
struct CustomerReservationLocalDetailView_Previews: PreviewProvider {
    static var previews: some View {
        CustomerReservationLocalDetailView()
    }
}
*/
