//
//  CustomerReservationsListView.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 30/11/21.
//

import SwiftUI

struct CustomerReservationsLocalListView: View {

    @ObservedObject var customerReservationsLocal = CustomerReservationDetailsLocalViewModel()
    @State var numberOfReservations: Int = 0
        
    var body: some View {
        VStack {
            if numberOfReservations == 0 {
                Spacer()
                NoReservationsView()
                Spacer()
            }
            List {
                ForEach(customerReservationsLocal.savedLocalReservations.indices, id: \.self) { index in
                    NavigationLink {
                        CustomerReservationLocalDetailView(savedLocalReservation: customerReservationsLocal.savedLocalReservations[index])
                    } label: {
                        HStack {
                            Text(customerReservationsLocal.savedLocalReservations[index].selectedDate!.toString)
                                .font(Font.body)
                                .foregroundColor(Color.lightBlack)
                        }
                    }
                    .listRowSeparator(.hidden)
                    .listRowInsets(EdgeInsets())
                }
                .onDelete(perform: self.deleteLocalReservation)
            }
            .listStyle(.plain)
            
        }
        .onAppear {
            numberOfReservations = customerReservationsLocal.fetchCustomerReservationsLocal()
        }
        .padding(.horizontal)
        .navigationTitle("Prenotazioni")
    }
    
    // MARK: - Functions
    // Delete local reservation
    private func deleteLocalReservation(at indexSet: IndexSet) {
        var deleted: Bool = false
        indexSet.forEach { index in
            let localReservation = customerReservationsLocal.savedLocalReservations[index]
            deleted = customerReservationsLocal.deleteCustomerReservationLocal(savedLocalReservations: localReservation)
        }
        if deleted {
            numberOfReservations = customerReservationsLocal.fetchCustomerReservationsLocal()
        }
    }
}

struct CustomerReservationsLocalListView_Previews: PreviewProvider {
    static var previews: some View {
        CustomerReservationsLocalListView()
    }
}
