//
//  SettingsLabelView.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 30/11/21.
//

import SwiftUI

struct SettingsLabelView: View {
    
    var title: String
    var image: String
    
    var body: some View {
        VStack {
            HStack {
                Text(title)
                    .font(Font.body)
                Spacer()
                Image(systemName: image)
            }
            Divider()
                .padding(.vertical, 4)
        }
    }
}
