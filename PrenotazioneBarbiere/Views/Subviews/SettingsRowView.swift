//
//  SettingsRowView.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 30/11/21.
//

import SwiftUI

struct SettingsRowView: View {
    
    var icon: String
    var text: String
    var color: Color
    
    var body: some View {
        HStack {
            ZStack {
                RoundedRectangle(cornerRadius: 8, style: .circular)
                    .fill(color)
                    .opacity(0.8)
                Image(systemName: icon)
                    .font(.title3)
                    .foregroundColor(.white)
            }
            .frame(width: 33, height: 33)
            Text(text)
                .font(Font.settingsRow)
                .foregroundColor(Color.lightBlack)
            Spacer()
            Image(systemName: "chevron.right")
                .font(.headline)
                .foregroundColor(Color.lightBlack)
        }
    }
}
