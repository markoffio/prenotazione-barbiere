//
//  SelectedServiceCellRow.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 09/10/21.
//

import SwiftUI

struct SelectedServiceCellRow: View {
    // MARK: - Properties
    // Service selected
    @State private var selected: Bool = false
    // Service
    @Binding var servicesToSelect: [String: Bool]
    // Service name
    let name: String
    
    var body: some View {
        HStack {
            Text(name)
                .font(Font.body)
                .padding(.leading)
                .padding([.top, .bottom], 20)
            Spacer()
            Image(systemName: selected ? "circle.fill" : "circle")
                .resizable()
                .frame(width: 18.0, height: 18.0)
                .foregroundColor(Color.lightBlue)
                .onTapGesture {
                    selected.toggle()
                    if selected { self.servicesToSelect.updateValue(false, forKey: name) }
                    else { self.servicesToSelect.removeValue(forKey: name) }
                    dump(self.servicesToSelect)
                }
        }
    }
}

/*
struct SelectedServiceCellRow_Previews: PreviewProvider {
    static var previews: some View {
        SelectedServiceCellRow()
    }
}
*/
