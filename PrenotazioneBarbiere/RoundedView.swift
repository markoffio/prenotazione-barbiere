//
//  RoundedButton.swift
//  Parco
//
//  Created by Marco Margarucci on 04/08/21.
//

import SwiftUI

struct RoundedView: View {
    // Button text
    var text: String = ""
    // Background color
    var backgroundColor: Color
    // Foreground color
    var foregroundColor: Color
    
    var body: some View {
        HStack {
            Text(text)
                .font(Font.button)
                .fontWeight(.semibold)
                .foregroundColor(foregroundColor)
        }
        .frame(height: 50)
        .frame(maxWidth: .infinity)
        .background(backgroundColor)
        .foregroundColor(.white)
        .cornerRadius(25)
    }
}

struct RoundedButton_Previews: PreviewProvider {
    static var previews: some View {
        RoundedView(text: "AVANTI", backgroundColor: Color.lightBlue, foregroundColor: .white)
    }
}
