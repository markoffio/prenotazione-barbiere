//
//  Service.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 05/10/21.
//

import Firebase
import FirebaseFirestoreSwift

struct Service: Identifiable, Codable {
    @DocumentID var id: String?
    var name: String
    var price: Int
    var category: String
}
