//
//  MonthInfo.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 03/03/22.
//

import Foundation
import FirebaseFirestoreSwift

struct MonthInfo: Identifiable, Decodable {
    @DocumentID var id: String?
    var days: [Int]
    var month: String
}
