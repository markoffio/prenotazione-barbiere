//
//  CustomerReservationDetailsLocal.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 03/12/21.
//

import Foundation
import UIKit

class CustomerReservationDetailsLocal: ObservableObject {
    // Customer id
    @Published var customerId: String = ""
    // Date
    @Published var date: Date = Date()
    // Selected date
    @Published var selectedDate: Date = Date()
    // Selected services
    @Published var selectedServices = [[String: Int]]()
    // Time
    @Published var time: String = ""
    // Total price
    @Published var totalPrice: Int = 0
    // QR code input string
    @Published var qrCodeInputString: String = ""    
}
