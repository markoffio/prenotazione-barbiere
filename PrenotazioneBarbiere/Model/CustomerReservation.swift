//
//  CustomerReservation.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 11/11/21.
//

import Foundation

import FirebaseFirestoreSwift

struct CustomerReservation: Identifiable, Codable {
    // MARK: - Properties
    
    // User id
    @DocumentID var id: String?
    // Customer full name
    let customerFullName: String
    // Customer id
    let customerId: String
    // Customer email
    let email: String
    // Reservation date
    let date: Date
    // Selected date
    let selectedDate: Date
    // Time
    let time: String
    // Selected services
    let selectedServices: [[String: Int]]
    // Total price
    let totalPrice: Int
}
