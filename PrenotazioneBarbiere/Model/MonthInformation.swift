//
//  MonthInformation.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 10/03/22.
//

import Foundation
import Firebase
import FirebaseFirestoreSwift

struct MonthInformation: Identifiable, Codable {
    @DocumentID var id: String?
    var timetables: [String]
    var isClosingDay: Bool
}
