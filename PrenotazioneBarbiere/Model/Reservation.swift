//
//  Reservation.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 05/10/21.
//

import SwiftUI

struct Reservation: Identifiable {
    var id = UUID().uuidString
    var date: Date = Date()
    var isFull: Bool
}

struct ReservationMatadata: Identifiable {
    var id = UUID().uuidString
    var reservation: [Reservation]
    var reservationDate: Date
}

func getDate(offset: Int) -> Date {
    let calendar = Calendar.current
    let date = calendar.date(byAdding: .day, value: offset, to: Date())
    return date ?? Date()
}



// Offset must be in the range [3...7]

var reservations: [ReservationMatadata] = [
    ReservationMatadata(reservation: [
        Reservation(isFull: true)
    ], reservationDate: getDate(offset: 5)),
    
    ReservationMatadata(reservation: [
        Reservation(date: Date(), isFull: true)
    ], reservationDate: getDate(offset: 3)),
    
    ReservationMatadata(reservation: [
        Reservation(date: Date(), isFull: true)
    ], reservationDate: getDate(offset: 7)),
]
