//
//  ReservationDetails.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 17/11/21.
//

import Foundation

class ReservationDetails: ObservableObject, Identifiable {
    var id = UUID()
    // Selected date
    @Published var selectedDate: Date = Date()
    // Time
    @Published var time: String = ""
    // Selected services
    @Published var selectedServices = [[String: Int]]()
}
