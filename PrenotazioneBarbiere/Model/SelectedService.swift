//
//  SelectedService.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 09/10/21.
//

import Foundation

enum ServiceCategory: String {
    case haircut
    case shave
    case wellness
}

struct SelectedService: Identifiable/*, Codable*/ {
    //@DocumentID var id: String?
    var id = UUID()
    // Local identifier
    let localID: String
    // Service category
    let category: ServiceCategory
    // Service name
    let name: String
    // Service selected
    let selected: Bool
}

let selectedServiceData = [
    SelectedService(localID: UUID().uuidString, category: .haircut, name: "Taglio classico", selected: false),
    SelectedService(localID: UUID().uuidString, category: .haircut, name: "Taglio alla moda", selected: false),
    SelectedService(localID: UUID().uuidString, category: .haircut, name: "Liscio totale", selected: false),
    SelectedService(localID: UUID().uuidString, category: .haircut, name: "Shampoo", selected: false),
    SelectedService(localID: UUID().uuidString, category: .haircut, name: "Taglio junior", selected: false),
    SelectedService(localID: UUID().uuidString, category: .shave, name: "Comfort Relax Shave", selected: false),
    SelectedService(localID: UUID().uuidString, category: .shave, name: "Luxury ritual shave", selected: false),
    SelectedService(localID: UUID().uuidString, category: .shave, name: "Contour B&M", selected: false),
    SelectedService(localID: UUID().uuidString, category: .shave, name: "Therapy B&M", selected: false),
    SelectedService(localID: UUID().uuidString, category: .shave, name: "Rasatura all'italiana", selected: false),
    SelectedService(localID: UUID().uuidString, category: .shave, name: "Modellatura barba", selected: false),
    SelectedService(localID: UUID().uuidString, category: .wellness, name: "Trattamenti specifici", selected: false),
    SelectedService(localID: UUID().uuidString, category: .wellness, name: "Peeling", selected: false)
]
