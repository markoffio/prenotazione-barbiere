//
//  TimeSlot.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 07/03/22.
//

import Foundation

import Foundation
import FirebaseFirestoreSwift

struct TimeSlot: Identifiable, Decodable {
    @DocumentID var id: String?
    var timetables: [String]
}
