//
//  HomeViewMessage.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 22/02/22.
//

import Foundation

struct HomeViewMessage {
    static let closedMessage: String = "Oggi siamo chiusi"
    static let selectDifferentDate: String = "Seleziona un'altra data"
    static let noTimeSlotsAvailable: String = "Tutto esaurito per oggi"
    static let hourPerDay: String = "Orari disponibili per il giorno"
    static let navigationTitle: String = "Prenota"
}
