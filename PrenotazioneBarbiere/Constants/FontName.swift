//
//  FontName.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 23/02/22.
//

import Foundation

struct FontName {
    static let regular: String = "LexendDeca-Regular"
    static let bold: String = "LexendDeca-Bold"
    static let semiBold: String = "LexendDeca-SemiBold"
}
