//
//  AuthenticationMessage.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 21/02/22.
//

import Foundation

struct AuthenticationMessage {
    // Full name
    static let fullNameError: String = "Questo campo non può essere vuoto."
    // Email
    static let noEmailAddressError: String = "Non hai inserito alcun indirizzo email."
    static let invalidEmailAddress: String =  "Indirizzo email non valido."
    // Password
    static let noPasswordError: String = "Non hai inserito alcuna password."
    static let invalidPasswordError: String = "Password non valida."
    static let invalidPasswordLength: String = "La password deve essere lunga almeno 8 caratteri."
    static let passwordNoUppercaseCharacter: String = "La password deve contenere almeno un carattere in maiuscolo."
    static let passwordNoLowercaseCharacter: String = "La password deve contenere almeno un carattere in minuscolo."
    static let passwordNoSpecialCharacter: String = "La password deve contenere almeno un carattere speciale."
    static let passowordNoNumber: String = "La password deve contenere almeno un numero."
    // Phone number
    static let noPhoneNumberError: String = "Non hai inserito alcun numero di telefono."
    static let invalidPhoneNumberError: String = "Numero di telefono non valido."
}
