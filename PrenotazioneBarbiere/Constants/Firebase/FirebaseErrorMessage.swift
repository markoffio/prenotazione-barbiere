//
//  FirebaseErrorMessage.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 21/02/22.
//

import Foundation

struct FirebaseErrorMessage {
    
    // MARK: - Network error messages
    
    struct Network {
        // Network error
        static let networkError: String = "Network error. Please try again later !"
    }
    
    // MARK: - Authetication error messages
    
    struct Authentication {
        // User not found
        static let userNotFound: String = "Account not found for the specified user. Please check and try again !"
        // User disabled
        static let userDisabled: String = "Your account has been disabled. Please contact support !"
        // Invalid email addres, invalid sender, invalid recipient email
        static let invalidEmail: String = "Please enter a valid email !"
        // Wrong password
        static let wrongPassword: String = "Your password or email is incorrect"
    }
    
    // MARK: - Create user error messages
    struct CreateUser {
        // Email already in use
        static let emailAlreadyInUse: String = "The email is already in use. Pick up another email !"
        // Weak password
        static let weakPassword: String = "Your password is too weak. The password must be 6 characters long or more !"
    }
    
    // MARK: - General error messages
    struct Unkwnown {
        // Unkwnown reason error
        static let unknown: String = "Unknown error, something went wrong. Try again later !"
    }
}

