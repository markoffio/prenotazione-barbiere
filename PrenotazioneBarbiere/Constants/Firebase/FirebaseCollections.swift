//
//  FirebaseCollections.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 21/02/22.
//

import Foundation
import Firebase

struct FirebaseCollections {
    // Customsers collection
    struct Customers {
        static let collectionName = Firestore.firestore().collection("customers")
        static let email: String = "email"
        static let fullName: String = "fullName"
        static let phoneNumber: String = "phoneNumber"
    }

    // Services collection
    struct Services {
        static let collectionName = Firestore.firestore().collection("services")
    }
    
    // Reservations collection
    struct Reservations {
        static let collectionName = Firestore.firestore().collection("reservations")
    }
    
    // Preferred services
    struct Preferred {
        static let collectionName = Firestore.firestore().collection("preferred")
    }
    
    // Closed days per month
    struct ClosedDaysPerMonth {
        static let collectionName = Firestore.firestore().collection("closedDaysPerMonth")
    }
    
    // Timeslots per day
    struct TimeslotsPerDay {
        static let collectionName = Firestore.firestore().collection("timeSlotsPerDay")
    }
    
    // Month information
    struct MonthInformation {
        static let collectionName: String = "monthInformation"
    }
}
