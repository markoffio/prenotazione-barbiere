//
//  CustomerReservationMessage.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 21/02/22.
//

import Foundation

struct CustomerReservationMessage {
    static let createCustomerReservationError: String = "Devi selezionare almeno un servizio prima di effettuare la prenotazione"
}
