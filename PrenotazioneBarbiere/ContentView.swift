//
//  ContentView.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 12/09/21.
//

import SwiftUI

struct ContentView: View {
    // MARK: - Properties
    
    // View model
    @EnvironmentObject var viewModel: AuthenticationViewModel
    // Network manager
    @ObservedObject var networkManager = NetworkManager()
    
    var body: some View {
        Group {
            if networkManager.isConnected {
                Group {
                    // If the user is already logged in then show the main view
                    if viewModel.customerSession != nil { MainView() }
                    else { LoginView() }
                }
            } else {
                // Show failed connection screen
                NoNetworkConnectionView()
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
