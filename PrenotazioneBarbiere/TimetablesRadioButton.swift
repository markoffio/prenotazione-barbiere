//
//  TimeSlotsRadioButton.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 12/11/21.
//

import SwiftUI

struct TimetablesRadioButton: View {
    
    @Environment(\.colorScheme) var colorScheme
    
    let id: String
    let callback: (String)->()
    let selectedId : String
    let size: CGFloat
    let color: Color
    let textSize: CGFloat
    
    init(_ id: String, callback: @escaping (String)->(), selectedId: String, size: CGFloat = 20, color: Color = Color.primary, textSize: CGFloat = 14) {
        self.id = id
        self.size = size
        self.color = color
        self.textSize = textSize
        self.selectedId = selectedId
        self.callback = callback
    }
    
    var body: some View {
        
        Button{
            self.callback(self.id)
        } label: {
            ZStack {
                RoundedRectangle(cornerRadius: 20)
                    .strokeBorder(Color.lightBlack, lineWidth: 1)
                    .background(RoundedRectangle(cornerRadius: 20).fill(self.selectedId == self.id ? Color.lightBlack : .clear))
                    .frame(height: 30)
                Text(id)
                    .font(Font.body)
                    .fontWeight(.semibold)
                    .foregroundColor(self.selectedId == self.id ? .white : Color.lightBlack)
                    .frame(width: 50)
                    .padding()
            }
        }
    }
}

struct Timetables: View {
    
    let items : [String]
    @State var selectedId: String = ""
    @Binding var itemsCount: Int
    @Binding var selected: Bool
    let callback: (String) -> ()
    @State var attempts: Int = 0
    
    var body: some View {
        ScrollView(.horizontal, showsIndicators: false) {
            HStack {
                ForEach(0..<itemsCount, id: \.self) { index in
                    if selected {
                        TimetablesRadioButton(self.items[index], callback: self.radioGroupCallback, selectedId: self.selectedId)
                    } else {
                        if index == 0 {
                            TimetablesRadioButton(self.items[index], callback: self.radioGroupCallback, selectedId: self.selectedId)
                                .onAppear {
                                    withAnimation(.spring(dampingFraction: 2).delay(0.2).repeatForever()) {
                                        attempts += 1
                                    }
                                }
                                .modifier(Shake(animatableData: CGFloat(attempts)))
                        } else {
                            TimetablesRadioButton(self.items[index], callback: self.radioGroupCallback, selectedId: self.selectedId)
                        }
                    }
                }
            }
            .padding()
        }
    }
    
    func radioGroupCallback(id: String) {
        selectedId = id
        callback(id)
    }
}
