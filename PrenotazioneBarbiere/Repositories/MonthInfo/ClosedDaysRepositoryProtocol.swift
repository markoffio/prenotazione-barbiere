//
//  MonthInfoRepositoryProtocol.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 03/03/22.
//

import Foundation

protocol MonthInfoRepositoryProtocol {
    func fetchAll(completion: @escaping (Result<[MonthInfo]?, Error>) -> Void)
}
