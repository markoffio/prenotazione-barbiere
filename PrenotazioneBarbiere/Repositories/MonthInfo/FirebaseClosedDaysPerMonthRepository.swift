//
//  FirebaseMonthInfoRepository.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 03/03/22.
//

import Foundation
import Firebase
import FirebaseFirestore
import FirebaseFirestoreSwift

class FirebaseMonthInfoRepository: MonthInfoRepositoryProtocol {
    
    // Fetch month info
    func fetchAll(completion: @escaping (Result<[MonthInfo]?, Error>) -> Void) {
        FirebaseCollections.ClosedDaysPerMonth.collectionName
            .getDocuments { snapshot, error in
            guard let snapshot = snapshot, error == nil else {
                completion(.failure(error ?? NSError(domain: "[snapshot is nil]", code: 100, userInfo: nil)))
                return
            }
            let monthInfos: [MonthInfo] = snapshot.documents.compactMap { document in
                var monthInfo = try? document.data(as: MonthInfo.self)
                if monthInfo != nil { monthInfo!.id = document.documentID }
                return monthInfo
            }
            completion(.success(monthInfos))
        }
    }
}
