//
//  FirebaseTimeslotsPerDayRepository.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 07/03/22.
//

import Foundation
import Firebase
import FirebaseFirestore
import FirebaseFirestoreSwift

class FirebaseTimeslotsPerDayRepository: TimeslotsPerDayRepositoryProtocol {

    // Fetch time slots per day
    func fetchAll(completion: @escaping (Result<[TimeSlot]?, Error>) -> Void) {
        FirebaseCollections.TimeslotsPerDay.collectionName
            .getDocuments { snapshot, error in
            guard let snapshot = snapshot, error == nil else {
                completion(.failure(error ?? NSError(domain: "[snapshot is nil]", code: 100, userInfo: nil)))
                return
            }
            let monthInfos: [TimeSlot] = snapshot.documents.compactMap { document in
                var monthInfo = try? document.data(as: TimeSlot.self)
                if monthInfo != nil { monthInfo!.id = document.documentID }
                return monthInfo
            }
            completion(.success(monthInfos))
        }
    }
}
