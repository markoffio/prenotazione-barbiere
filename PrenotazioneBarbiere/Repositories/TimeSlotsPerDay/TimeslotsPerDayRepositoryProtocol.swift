//
//  TimeslotsPerDayRepositoryProtocol.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 07/03/22.
//

import Foundation

protocol TimeslotsPerDayRepositoryProtocol {
    func fetchAll(completion: @escaping (Result<[TimeSlot]?, Error>) -> Void)
}
