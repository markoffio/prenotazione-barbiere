//
//  MonthInformationFirebaseRepository.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 10/03/22.
//

import Foundation
import Firebase
import FirebaseFirestoreSwift

class MonthInformationFirebaseRepository {
    private var db: Firestore
    
    init() {
        db = Firestore.firestore()
    }
    
    func getMonthInformation(completion: @escaping(Result<[MonthInformation]?, Error>) -> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) { [self] in
            db.collection(FirebaseCollections.MonthInformation.collectionName)
                //.document("aprile")
                .document("maggio")
                .collection("timetables")
                .getDocuments { querySnapshot, error in
                    guard let querySnapshot = querySnapshot, error == nil else {
                        completion(.failure(error ?? NSError(domain: "Failed", code: 103, userInfo: nil)))
                        return
                    }
                    let monthInformations: [MonthInformation]? = querySnapshot.documents.compactMap { document in
                        var monthInformation = try? document.data(as: MonthInformation.self)
                        if monthInformation != nil { monthInformation!.id = document.documentID }
                        return monthInformation
                    }
                    completion(.success(monthInformations))
                }
        }
    }
}
