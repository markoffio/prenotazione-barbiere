//
//  FirebaseServiceRepository.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 10/11/21.
//

import Foundation
import Firebase
import FirebaseFirestore
import FirebaseFirestoreSwift

class FirebaseServiceRepository: ServiceRepositoryProtocol {
    
    // Fetch all services
    func fetchAll(completion: @escaping (Result<[Service]?, Error>) -> Void) {
        FirebaseCollections.Services.collectionName
            .order(by: "category", descending: false)
            .getDocuments { snapshot, error in
            guard let snapshot = snapshot, error == nil else {
                completion(.failure(error ?? NSError(domain: "[snapshot is nil]", code: 100, userInfo: nil)))
                return
            }
            let services: [Service] = snapshot.documents.compactMap { document in
                var service = try? document.data(as: Service.self)
                if service != nil { service!.id = document.documentID }
                return service
            }
            completion(.success(services))
        }
    }
}
