//
//  ServiceRepositoryProtocol.swift
//  PrenotazioneBarbiere
//
//  Created by Marco Margarucci on 10/11/21.
//

import Foundation

protocol ServiceRepositoryProtocol {
    func fetchAll(completion: @escaping (Result<[Service]?, Error>) -> Void)
}
